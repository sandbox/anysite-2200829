(function($){

EA_panel = {
	plural : {
		'comment' : [Drupal.t('comment'),Drupal.t('comments')],
		'node' : [Drupal.t('node'),Drupal.t('nodes')],
		'menu' : [Drupal.t('menu'),Drupal.t('menus')]
		},
	pluralByKey : function(count,key){
		if(typeof this.plural[key] == 'undefined' || !this.plural[key].length)return false;
		if(this.plural[key].length==1)
			this.plural[key].push(this.plural[key][0]);
		return count == 1 ? this.plural[key][0] : this.plural[key][1];
		},
	deleteLink : function(mlid){
		if(typeof mlid != 'object')
			var toDelete = $('#EA-menu-part-' + mlid);
		else var toDelete = mlid;
		var	moveTo = toDelete.closest('ul.EA-menus-parts')
		toDelete.find('.EA-menu-part').each(function(){
			moveTo.append($(this));
			});
		toDelete.remove();
		},
	changeNewLinkItem : function(item){
		var htmlElement = $('#' + item.new);
		if(!htmlElement.length){
			//console.error('item with ID: ' + item.new +  ' not found');
			return;
			}
		else{
			htmlElement.attr('id','EA-menu-part-' + item.mlid);
			var elmClass = htmlElement.attr('class');
			elmClass = elmClass.replace(item.new, 'EA-menu-part-' + item.mlid);
			htmlElement.attr('class',elmClass);
			htmlElement.find('.EA-menu-part-menu-data-wrp').removeClass('EA-stat-added');
			var mlid = htmlElement.find('.EA-menu-part-menu-data-wrp:first .EA-hidden .mlid');
			if(!mlid.length)
				 htmlElement.find('.EA-menu-part-menu-data-wrp:first .EA-hidden').append("<div class='mlid'>" + item.mlid  + "</div>")
			else mlid.text(item.mlid );
			//This function is out of object scope so we need to do it manually
			var dataItem =  htmlElement.find('.EA-menu-part-menu-data-wrp:first'),
				data = dataItem.data('EAData'),
				dataOrig = dataItem.data('EADataOrig');
			data.mlid = item.mlid;
			dataOrig.mlid = item.mlid;
			dataItem.data('EADataOrig',dataOrig);
			dataItem.data('EAData',data);
		
			}
		},
	addNewItem : function(appendTo,curObj,clicked){
		//console.log(curObj)
		var	conf  = {
			submit:function(e){
				e.preventDefault();
				var dataSend = $(this).mySerializeForm('link_browser');
				//dataSend = $.extend(dataSend,addData);
				dataSend['menu_name'] = curObj.parent.menuObj.menuName;
				if(clicked)
					dataSend['plid'] = curObj.getId(curObj.getDataObj(clicked));//TODO menuName
				else dataSend['plid'] = 0;
				delete(dataSend.call_views);
				delete(dataSend.ajaxed);
				$.ajax({
					url: easyAdminAjaxPrefix + 'easy_admin/add_menu_item',
					data:{link:dataSend},
					type: "POST",
					success:function(response){
						//console.log(response)
						var oldItem = appendTo,//clicked.closest('.EA-menu-part'),
							newItem = $(response.html);//oldItem.clone(),	
						if(clicked){
							depth = curObj.findDepth(oldItem);
							if(depth + 1 > curObj.parent.elements.sortableOptions.maxLevels){
								var minus = depth + 1 - curObj.parent.elements.sortableOptions.maxLevels;
								//console.log(minus,oldItem)
								for(var i = 0;i < minus; i++)
									oldItem = oldItem.parent().closest('.EA-menu-part');
								//console.log(oldItem)
								if(!oldItem)
									oldItem = curObj.parent.parnet.mainContainer.find('.EA-menu-part:first')
								if(!oldItem.find('ul.EA-menus-parts').length)
									oldItem.append("<ul class='EA-menus-parts'></ul>");
								oldItem = oldItem.find('ul.EA-menus-parts');
								}
							}
						
						newItem.find(">ul").remove();
						var dataHtml = newItem.find('.EA-menu-part-data-wrp');
						dataHtml.find('.EA-hidden .mlid').text('0');
						//console.log(oldItem);
						oldItem.append(newItem);
						var dataContainer = newItem.find('.EA-menu-part-menu-data-wrp');
						curObj.markAs('added',dataContainer);
						curObj.load();
						easyAdminLinkBrowser.close();
						}
					});
				} 
			};
			//console.log(conf.submit)
		easyAdminLinkBrowser.load({},conf);
		},
	getAllData: function(){
		var allData = {},
			deleted = false;
		$('.EA-menu-edit,.EA-comments-view',$('#EA-menu-edit-all,#EA-comments-window')).each(function(){
			var sorted = $(this).is('.EA-stat-sorted');
			console.log(this,sorted)
			$('.EA-stat-changed',$(this)).each(function(){
				//console.log($(this))
				var data = $(this).data('EAData');
				var name = data['EA-name'], 
					id = data[data['EA-id']],
					module = data['EA-module'];
				if($(this).is('.EA-menu-part-menu-data-wrp') && sorted ){
					var itemParent = $(this).closest('.EA-menu-part');
					data['plid'] = itemParent.parent().closest('.EA-menu-part').attr('id');
					
					if(data['plid'])data['plid'] = data['plid'].replace('EA-menu-part-','');
					else data['plid'] = 0;
					data['weight'] = itemParent.siblings().andSelf().index(itemParent);
					}
					
				if(!name)return;
				if(typeof allData[module] == 'undefined')allData[module] = {};
				if(typeof allData[module].save == 'undefined')allData[module].save = {};
				if(typeof allData[module].save[name] == 'undefined')allData[module].save[name] = {};
				if(allData[module].save[name][id] == 'undefined')allData[module].save[name][id]= {};
				allData[module].save[name][id] = $.extend(true,allData[module].save[name][id],data);
				});
			$('.EA-stat-deleted',$(this)).each(function(){
				deleted = true;
				//if(typeof deleted == 'undefined')
				//	deleted.comments = Array();
				var data = $(this).data('EAData');
				var name = data['EA-name'], 
					id = data[data['EA-id']],
					module = data['EA-module'];
				if(!name)return;
				if(typeof allData[module] == 'undefined')allData[module] = {};
				if(typeof allData[module].delete == 'undefined')allData[module].delete = {};
				if(typeof allData[module].delete[name] == 'undefined')allData[module].delete[name] = [];
				allData[module].delete[name].push(id);
				/*deleted.comments.push({
					id: id,
					title : data['subject']
					});*/
				});
			$('.EA-stat-added',$(this)).each(function(){
				var data = $(this).data('EAData');
				data['new'] = $(this).closest('.EA-menu-part').attr('id');
				data['menu_name'] = $(this).closest('.EA-menu-edit').attr('id').replace('EA-menu-edit-','');
				if(!data['menu_name'])return;
				var name = data['EA-name'],
					 id = data['new'],
					 module = data['EA-module'];
				if($(this).is('.EA-menu-part-menu-data-wrp') ){
					var itemParent = $(this).closest('.EA-menu-part');
					data['plid'] = itemParent.parent().closest('.EA-menu-part').attr('id');
					//console.log(data['plid'])
					if(data['plid'])data['plid'] = data['plid'].replace('EA-menu-part-','');
					else data['plid'] = 0;
					data['weight'] = itemParent.siblings().andSelf().index(itemParent);
					//console.log(data)
					}
				
				//console.log(data['new'],id,$(this).closest('.EA-menu-part').attr('id'))
				if(!name)return;
				if(typeof allData[module] == 'undefined')allData[module] = {};
				if(typeof allData[module].save == 'undefined')allData[module].save = {};
				if(typeof allData[module].save[name] == 'undefined')allData[module].save[name] = {};
				if(allData[module].save[name][id] == 'undefined')allData[module].save[name][id]= {};
				allData[module].save[name][id] = $.extend(true,allData[module].save[name][id],data);
				});
			if(sorted)
				$('.EA-menu-part-menu-data-wrp').not('.EA-stat-added,.EA-stat-changed,.EA-stat-deleted').each(function(){
					var data = $(this).data('EAData');
						data.justSort= true;
					var name = data['EA-name'], 
						id = data[data['EA-id']],
						module = data['EA-module'];
					if($(this).is('.EA-menu-part-menu-data-wrp') && sorted ){
						var itemParent = $(this).closest('.EA-menu-part');
						data['plid'] = itemParent.parent().closest('.EA-menu-part').attr('id');
						//console.log(data['plid'])
						if(data['plid'])data['plid'] = data['plid'].replace('EA-menu-part-','');
						else data['plid'] = 0;
						data['weight'] = itemParent.siblings().andSelf().index(itemParent);
	//					//console.log(data['weight'],itemParent,itemParent.closest('.EA-menu-part'))
						}
						
					if(!name)return;
					if(typeof allData[module] == 'undefined')allData[module] = {};
					if(typeof allData[module].save == 'undefined')allData[module].save = {};
					if(typeof allData[module].save[name] == 'undefined')allData[module].save[name] = {};
					if(allData[module].save[name][id] == 'undefined')allData[module].save[name][id]= {};
					allData[module].save[name][id] = $.extend(true,allData[module].save[name][id],data);
					});
			});
		//console.log(allData);
		//$('.EA-comments-view',$('#EA-comments-window'))
		var form = $('#ea-edit-menu-item-form');
		var sendData = {
			alldata:allData,
			action:'all',
			form_build_id : form.find("[name=form_build_id]").val(),
			form_token : form.find("[name=form_token]").val(),
			form_id :form.find("[name=form_id]").val()
			};		
		
		var confText = '';
		if(deleted){
			for(var x in sendData.alldata){
				if(typeof sendData.alldata[x].delete != 'undefined'){
					var allDeletes = sendData.alldata[x].delete;
					for(var xx in allDeletes){
						var translate = this.pluralByKey(allDeletes[xx].length,xx), //trying to find translted name
							name = translate ? translate : xx;	
							confText += '<li>' + allDeletes[xx].length + ' ' + name + '</li>';
						/*confText += '<li>' +Drupal.formatPlural(allDeletes[xx].length,'@count @name part','@count @name parts',{
							'@count' : allDeletes[xx].length,
							'@name' : name
							}) +'</li>'; */
						}
					if(confText)
					 confText = "<h3>Warning! you are going to delete:</h3><ul>" + confText + "</ul><div>Are you sure? </div>";
					};
				}
			}
		for(var x in EA_data_store){
			var save = function(){
			EA_data_store[x].ajaxWrapper({
			data:sendData,
			successCallback : function(data){
				//if(JSON.stringify(EAPanelResponseFunc) == "{}")return;
				for(x in data){
					if(typeof EAPanelResponseFunc[x] == 'function')
						EAPanelResponseFunc[x](data[x]);
					}
				}
			});
			};
			//	buttons = {};
			//buttons[Drupal.t('Yes')]	: function()
			if(confText){
				$("<div>" + confText + "</div>").dialog({
					title:Drupal.t('Delete warning'),
					buttons : [
						 {
						 text : Drupal.t('yes'),
						 click:function(){
							 $(this).dialog('destroy');
							 save();
							 }
						 },
						{
						text : 	Drupal.t('No'),
						click:function(){
							 $(this).dialog('destroy');
							 }
							} 
						]
					});
				}
			else save();
			break;
			}
		
		},
	panelShowlResponse : function(data){
		//console.log(data)
		for(operation in data){
			var op = data[operation];
			for(itemType in op){
				//console.log(itemType);
				var results = op[itemType]['messages'],
					messages = op[itemType]['panel_messages'];
				if(typeof messages == 'undefined')continue;
				//console.log(op[itemType])					
				for(var x = 0; x < messages.length; x++){
					var msg = messages[x];
					//console.log(msg);
					if(!msg.type)msg.type = 'message;'
					msg = Drupal.theme("EA_panel_message",msg.title,msg.type,msg.description);
					easyAdminPanel.print(msg);
					}
				for(resultType in results){
				if(!results[resultType].length)continue;
					
					var allResults = results[resultType];
					for(var x = 0; x < allResults.length; x++){
						//console.log(itemType,allResults[x])
						if(itemType == 'menu'){
							var item = allResults[x].item;
							//description += "<li><strong>" + Drupal.t("Item number") + ":</strong> item.mlid<br/><strong>" + Drupal.t('Title') + ":</strong> "+ item.title+ "</li>";
							if(typeof item.new !== 'undefined'){
								EA_panel.changeNewLinkItem(item);
								}
							if(operation == 'delete'){
								EA_panel.deleteLink(item.mlid);
								//console.log($('#EA-menu-part-' + item.mlid))
								}
							else if(operation == 'save'){
								$('#EA-menu-part-' + item.mlid + ' .EA-menu-part-menu-data-wrp').removeClass('EA-stat-changed');
								}
							}
						else if(itemType == 'node'){
							//console.log(allResults[x]);
							var item = allResults[x];
							//description += "<li><strong>" + Drupal.t("Item number") +":</strong> "+item.nid+"<br/><strong>" + Drupal.t("Title") + ":</strong> " + item.title + "</li>";
							if(operation == 'save'){
								$('.EA-menu-part-node-' + item.nid ).removeClass('EA-stat-changed');
								}
							else if(operation == 'delete'){
								EA_panel.deleteLink($('.EA-menu-part-node-' + item.nid ).closest('.EA-menu-part'));
								}
							}
						else if(itemType == 'comment'){
							//console.log('ssss')
							var item = allResults[x];
							//description += "<li><strong>" + Drupal.t("Item number") +":</strong> "+item.nid+"<br/><strong>" + Drupal.t("Title") + ":</strong> " + item.title + "</li>";
							if(operation == 'delete'){
								$('.EA-cid-' + item.cid).remove();
							//	//console.log(item);
								}
							else if(operation == 'save'){
								$('.EA-cid-' + item.cid).removeClass('EA-stat-changed');
								//console.log("comm");
								}
							}
						else if(itemType == 'term'){
							var item = allResults[x];
							//description += "<li><strong>" + Drupal.t("Item number") +":</strong> "+item.nid+"<br/><strong>" + Drupal.t("Title") + ":</strong> " + item.title + "</li>";
							if(operation == 'delete'){
								EA_panel.deleteLink($('.EA-tid-' + item.tid).closest('.EA-menu-part'));
								//console.log(item);
								}
							else if(operation == 'save'){
								//console.log($('.EA-tid-' + item.tid).removeClass('EA-stat-changed'));
								
								}
							}
						else if(itemType == 'view'){
							var item = allResults[x];
							if(operation == 'save'){
								$('.EA-menu-part-view-'+ item.view + '.EA-menu-part-display-' +  item.display).removeClass('EA-stat-changed');
								}
							}	
						}
					/*description += "</ul>";
					if(description == "<ul></ul>")description = false;
					var showType = resultType == 'failed' ? "error" : "message";
					easyAdminPanel.print(Drupal.theme("EA_panel_message",message,showType,description));*/
					
					}
				
				}
			}
		$('.EA-stat-deleted').each(function(){//remove new item delted before saving
			if($(this).closest('.EA-menu-part').is("[id^='EA-menu-part-not-saved']"))
				EA_panel.deleteLink($(this).closest('.EA-menu-part'));
			})
		
		if(!$("[class*='EA-stat-']").length){
			$(window).off('beforeunload');
			$('.EA-save-all').hide();
			}
		$('.EA-menu-edit,.EA-comments-view',$('#EA-menu-edit-all,#EA-comments-window')).each(function(){
			$(this).removeClass('EA-stat-sorted');
			});
		}


	
	}
		

EA_data_store_obj = function(mainContainer,parent){
	if(typeof mainContainer == 'undefined'){
		//console.error("No mainContainer specified. abort.");
		return;
		}
	this.parent = parent;
	var curObj = this;
	this.keys = {
		dataKey : "EA-data",
		dataKeyOrig : "EA-data-orig",
		dataHtmlOrig : 'EA-html-orig',
		dataObj : ".EA-data-wrp",
		dataObjStore : ".EA-data",
		dataAttrPre : "EA-dataClass-"
		};
	this.stepData =  {
		create : {},
		save : {},
		delete : {},
		order : {}
		};
	this.allData = [this.stepData];
	this.currentData = {};
	this.currentDataNum = 0;
	this.changeData = function(action,type,id,data){
		/*for(var x = (allData.length -1); x--;x<=0;){
			if(allData[x] == this.currentData){
				allData.splice(x+1);
				}
			}*/
		this.allData.splice(this.currentDataNum +1);
		var oldData = this.allData[this.allData.length-1];
		var newData = $.extend(true,{},oldData);
		newData.html = this.mainContainer.html();
		//console.log(newData)
		if(typeof newData[action][type] == 'undefined')
			newData[action][type] = {};
		if(typeof newData[action][type][id] == 'undefined')
			newData[action][type][id] = {};
		for(var x in data){
			newData[action][type][id][x] = data[x];
			}
		this.allData.push(newData);
		this.currentData = newData;
		this.currentDataNum = this.allData.length -1;
		
		};
	this.revertData = function(){
		this.currentDataNum--;
		if(this.currentDataNum >= 0)
			this.currentData = this.allData[this.currentDataNum];
		else{
			this.currentData = {html:this.htmlOrigin};
			this.allData= [];
			}
		this.refresh();
		};
	this.forwardData =  function(){
		this.currentDataNum++;
		if(this.currentDataNum <= this.allData.length -1)
			this.currentData = this.allData[this.currentDataNum];
		this.refresh();
		};
	this.refresh = function(){
		if(this.currentData.html){
			this.mainContainer(this.currentData.html);
			}
		this.load();
		};
	this.refreshValues = function(){
		var curObj = this;
		$('EA-changed').each(function(){
			curObj.refreshValue($(this));
			});
		};
	this.refreshValue = function(obj){
		var type = this.findType();
		var id = this.findId();
		var data = this.currentData;

		};
	this.findDepth = function(element){
		var depth = 1,
			checkElm = element;
		//console.log(checkElm)
		while(checkElm.parent().closest('.EA-menu-part').length && depth < 9){
				//console.log(checkElm)
			checkElm = checkElm.parent().closest('.EA-menu-part');
			depth++
			}
		return depth;
		}; 
	this.load  = function(){
		//console.log(curObj);
		//console.log($(curObj.keys.dataObj));
		
		$(curObj.keys.dataObj).each(function(){
			if($(this).data(curObj.keys.dataKeyOrig))return;
			var data = {};
			curObj.dataFromDomAll($(this),data);
			//console.log(data)
			$(this).data(curObj.keys.dataKeyOrig,$.extend(true,{},data));
			$(this).data(curObj.keys.dataKey,data);
			$(this).data(curObj.keys.dataHtmlOrig,$(this).html());
			});
		};
	this.backToStart = function(obj){
		var dataObj = this.getDataObj(obj),
			origData = $.extend(true,{},dataObj.data(this.keys.dataKeyOrig)),
			origHtml = dataObj.data(curObj.keys.dataHtmlOrig)
			objClass = dataObj.attr('class');
		dataObj.data(curObj.keys.dataKey,origData);
		if(origHtml)dataObj.html(origHtml);
		if(objClass){
			objClass = 	objClass.replace(/EA-stat-.+/g,"").replace(/\s{2,}/," ");
			dataObj.attr('class',objClass);
			}
		if(!$("[class*='EA-stat-']").length){
			$(window).off('beforeunload');
			$('.EA-save-all').hide();
			}
			
		};
	this.openNodeEdit = function(e){
		e.preventDefault();
		//console.log('dddddd');
		//$(this).off('click.EA-node-link','.EA-link-edit-node',this.openNodeEdit);
		var thisLink = $(this),
			url = $(this).attr('href'),
			titleElm = $(this).closest('.EA-menu-part-node'),
			title = titleElm.find('.span-input').text() || titleElm.find('.span-input-input').val();
		if(title)title = Drupal.t("Edit @title",{'@title':title});
		var curIframe = new EA_global_helpers.iframe(url,function(elm,myId){
			var objId = curObj.getId(thisLink);
			//console.log(objId)
			$.ajax({url : easyAdminAjaxPrefix+ "easy_admin/ajax_refresh_link",
				data:{path:'node/' + objId},
				success : function(data){
					console.log(data,data[objId]);
					if('node/' + objId == data.link_path){
						var html = $("<div>" + data.html + "</div>").find('.EA-menu-part-extra').html();
		//				//console.log(thisLink.closest('.EA-menu-part-extra'),html)
						thisLink.closest('.EA-menu-part-extra').html(html);
						var dataObj = curObj.getDataObj(thisLink);
						var data = {};
						//curObj.dataFromDomAll(dataObj,data);
					//	//console.log(dataObj,data);
						dataObj.data(curObj.keys.dataKey,data);
						curIframe.close();
						}
					},
				dataType:'json',
				type : 'POST'
				});
			},{
				width:400,
				title : title
				});
		curIframe.open();
		return false;
		};
	this.init = function(){
		this.htmlOrigin = this.mainContainer;
		var curObj = this;
		//console.log($(this.keys.dataObj).length)
		this.load();
		
		$(document).off('EA_change_url').on('EA_change_url',function(e){		
			var container = $("#" + e.message);
			var data_wrp = container.find('.EA-data-wrp:first');
			var extra_wrp = container.find('.EA-menu-part-extra');	
			var path = curObj.getDataFromDataObj(data_wrp,'link_path');
			//console.log(e.message,container,data_wrp,extra_wrp)
			
			$.ajax({url : easyAdminAjaxPrefix + "easy_admin/ajax_refresh_link",
				data:{path:path},
				success : function(data){
					console.log('b',data)		
					var htmlAll = $("<div>" + data.html + "</div>");
					var extraHtml = htmlAll.find('.EA-menu-part-extra'),
						arrowHtml = htmlAll.find('.EA-menu-part-separator');
					if(extra_wrp.length){
						extra_wrp.html(extraHtml.html());
						console.log(extraHtml.find('.EA-break-word'));
						}
					else{
						data_wrp.after(arrowHtml).next().after(extraHtml);
						extra_wrp = data_wrp.siblings('.EA-menu-part-extra');
						//console.log(extra_wrp,extra_wrp.length)
						}	
					var data = {};
					curObj.dataFromDomAll(data_wrp,data);
					extra_wrp.data(curObj.keys.dataKey,data);	
					//TODO find if this ajax overriding changes in EXTRA node
					var dataObj = curObj.getDataObj(data_wrp);
					
					//console.log(dataObj,data);
					
					
					},
				dataType:'json',
				type : 'POST'
				});
			//console.log(data_wrp,path);
			});
			
	//	//console.log(mainContainer)
		mainContainer.on('mouseenter','.span-input-wrp',function(){
			var span = $(this).find('.span-input');
			
			if(!span.length) return;
			var text = span.text().replace("&lt;","<").replace("&gt;",">"),
				elmClass= span.attr('class').replace('span-input',""),
				width = span.width();
			$(this).html($("<input class='span-input-input " + elmClass + "' type='text' value='"+text+"' />").data('orig',text).width(width));
			}).on('mouseout','.span-input-wrp',function(){
			var input = $(this).find('.span-input-input');
			if(!input.is(':focus')){
		//		//console.log(input)
				var text = $(this).find('.span-input-input').val().replace("<","&lt;").replace(">","&gt;"),
					elmClass = input.attr('class').replace('span-input-input',"");
				$(this).html("<span class='span-input " + elmClass + "' >" +text+"</span>");
				}
			}).on('blur','input',curObj.inputBlur).on('paste keydown','.span-input-input',function(){
				var input = $(this);
				var oldLength = input.val().length;
			//	//console.log(input,oldLength)
				setTimeout(function(){
					var newLength = input.val().length,
						newLengthWidth = input.width();
					newLengthWidth = (oldLength ? (newLength/oldLength) * newLengthWidth : newLengthWidth);
					newLengthWidth = newLengthWidth > 450? 450 :newLengthWidth;
					if(newLengthWidth)
						input.width(newLengthWidth);
					});
				}).on('focus','.EA-menu-part-link-path input',function(){
				var input = $(this);
				var objectParent = curObj.getDataObj($(this));
				var data = {
					title:curObj.data(objectParent,'link_title'),
					link_path:curObj.data(objectParent,'link_path')
					};
				var conf = {
					submit : function(e){
						input.trigger('blur').closest('div').trigger('mouseout');
						var linkDataParts = ['link_path','link_title'];
						var changed = false;
						for(var x =0;x < linkDataParts.length; x++){
							var value = $(this).find('[name=link_browser_' + linkDataParts[x] + ']').val();
							var oldVal = curObj.data(objectParent,linkDataParts[x]);
							curObj.data(objectParent,linkDataParts[x],value);
						//	//console.log(objectParent.find("." + curObj.keys.dataAttrPre + "data-" + linkDataParts[x]))
							if(oldVal != value){
								changed =true;
								if(linkDataParts[x]=='link_path'){
									var sendId = objectParent.closest('.EA-menu-part').attr('id') ;
									$.event.trigger({
										type : 'EA_change_url',
										message: sendId,
										time: new Date()
										});
									}					
								}
							var fill = objectParent.find("." + curObj.keys.dataAttrPre + "data-" + linkDataParts[x]);
						//	//console.log(fill,value);
							if(fill.is('span'))fill.text(value);
							else if(fill.is('input'))fill.val(value);
							}
						e.preventDefault();
						easyAdminLinkBrowser.close();
						//console.log(objectParent,objectParent.is('.EA-stat-added'))
						if(changed && objectParent.is(':not(".EA-stat-added")'))
							curObj.markAs('changed',objectParent);
						}
					}
				easyAdminLinkBrowser.load(data,conf);
				}).on('click','.EA-link-edit-node',this.openNodeEdit).on('click','.EA-node-toggle,.EA-link-toggle',function(){
			var name = curObj.getDataAttr($(this),'data'),	
			parentObj = curObj.getDataObj($(this));
			var oldValue = Number(curObj.data(parentObj,name));
			//console.log(oldValue,typeof oldValue);
			if(name == 'expanded' ){
				var action = !oldValue ?  "removeClass" :"addClass";
				$(this).closest('.EA-menu-part')[action]('EA-menu-part-not-expanded');
				}
			else if(name == 'hidden'){
				var action = !oldValue ?  "addClass": 'removeClass'  ;
				$(this).closest('.EA-menu-part')[action]('EA-menu-part-hidden');
				}
			
			curObj.data(parentObj,name,Number(!Number(oldValue)));
			var classToggle =  !oldValue ?  ["EA-on","EA-off"] : ["EA-off","EA-on"];
			//console.log(oldValue,classToggle);
			if(parentObj.is(':not(".EA-stat-added")'))
				curObj.markAs('changed',parentObj);
			$(this).addClass(classToggle[0]).removeClass(classToggle[1]);
			if($(this).is('.EA-link-toggle-comment.EA-dataClass-data-status')){
				var classToggle =  !oldValue ?  ["published","unpublished"] : ["unpublished","published"];
				parentObj.addClass(classToggle[0]).removeClass(classToggle[1]);
				}
			//console.log(curObj.data(parentObj,name));
			}).on('click','.icon-trash.EA-icon',function(){
				curObj.markAs('deleted',$(this).closest(curObj.keys.dataObj));
			}).on('click','.icon-undo.EA-icon',function(){
				curObj.backToStart($(this));
			}).on('click','.EA-menu-part-menu-data-wrp .icon-plus.EA-icon:first',function(){
				var parentObj = curObj.getDataObj($(this));
				var plid = Number(curObj.data(parentObj,"mlid"));
		//		//console.log(curObj)
				easyAdminLinkBrowser.load({mlid:0,menu_name:curObj.parent.menuObj.menuName});
				easyAdminLinkBrowser.elements.editBoxFormObj.append("<input class='remove-me' type='hidden' name=link_browser_plid' value='" + plid + "'/>");
			}).on('click','.EA-menu-part .icon-plus',function(){			
				var clicked = $(this);
				EA_panel.addNewItem($(this),curObj,clicked);
			}).on('click keydown',".EA-node.icon-comments",function(){
				var nid = curObj.getId($(this)),
					commentsSlector = ".EA-comments-view-"+ nid;
				if($(commentsSlector).length){
					$('#EA-comments-window .EA-comments-view').hide();
					$(commentsSlector).show();
					$('#EA-comments-window').dialog('open');
					return false;
					}
				$.post(easyAdminAjaxPrefix + "/easy_admin/ajax_get_comments/" + nid,function(data){
					if(!data)return;
					if(!$('#EA-comments-window').length){
						
						var commentsWindow = $("<div id='EA-comments-window'></div>");
						commentsWindow.dialog({
							title : Drupal.t('Administer comments'),
							open : function( event, ui ){
								var cssObj = {
									position : 'fixed',
									height : $(window).height(),
									top:0,
									},
									direction = Drupal.settings.easy_admin.dir  == '1' ? "left" : "right";
								cssObj[direction] = 0;
								if(direction != 'left')
									cssObj['left'] = 'inherit';
								//console.log(cssObj);
								$(this).closest('.ui-dialog').css(cssObj).end().css('height','100%');
								}
							});
						
						}
					$('#EA-comments-window').append(data);
					
					$('#EA-comments-window .EA-comments-view').hide();
					$('#EA-comments-window .EA-comments-view-' +nid).show();
					$('#EA-comments-window').dialog('open');
					if(typeof EA_PANEL_COMMENTS=='undefined'){
						EA_PANEL_COMMENTS = new EA_data_store_obj($('#EA-comments-window'),$('#EA-comments-window'));	
						EA_PANEL_COMMENTS.load();
						}
					
					});
					
				return false;
				});
		};;
		
		
	this.inputBlur = function(){
		if($(this).val()!=$(this).data('orig')){
			var name = curObj.getDataAttr($(this),'data'),
				parentObj = curObj.getDataObj($(this));
		//	//console.log(name,parentObj)
			curObj.data(parentObj,name,$(this).val());
			curObj.markAs('changed',parentObj);	
			if(name == 'link_path'){	
				}
			}
		};
	
	this.dataFromDom = function(elm,data){
		if(elm.is(".EA-jsoned")){
			elm.removeClass("EA-jsoned");
			try{data[elm.attr("class")] = JSON.parse(elm.text());}
			catch(e){}
			}
		else{data[elm.attr("class")] = elm.text()}
		//console.log(data)
		};
	this.dataFromDomAll = function(elm,data){
		elm.find(curObj.keys.dataObjStore + ">div").each(function(){
			curObj.dataFromDom($(this),data);
			});
		};
	this.getDataObj = function(obj){
		//console.log(this)
		//console.log(obj.closest(this.keys.dataObj))
		if(obj.is(this.keys.dataObj))return obj;
		else return obj.closest(this.keys.dataObj);
		};
	this.getDataAttr= function(elm,name){
		var myRE = new RegExp(this.keys.dataAttrPre + name + "\\S+");
		//console.log(myRE,elm.attr("class"))
		var results = myRE.exec(elm.attr("class"));
		if(results)return results[0].split("-").pop();
		else return false;
		};
	this.getDataFromDataObj = function(obj,key,value){
		
		var key = (typeof key != 'undefined') ? key : false ;
		return this.data(this.getDataObj(obj),key,value);
		};
	this.markAs = function(type,elm){
		var elmClass = elm.attr('class');		
		elmClass = elmClass.replace(/EA-stat-\S+/g,"")  + ' EA-stat-' + type;
		elmClass= elmClass.replace(/\s+/g," ");
//		//console.log(elmClass)
		elm.attr('class',elmClass);
		$('.EA-save-all').show();
		$(window).on('beforeunload', function(){
			var sum = $("[class*=EA-stat-]").length;
			if(sum)
				return Drupal.t('@sum changes were\'nt saved', {'@sum' : sum});
			});
		};
	this.data = function(elm,key,value){
		data = elm.data(this.keys.dataKey);
		if($.isEmptyObject(data))return false;
		if(!key) return data;
		if(typeof value == "undefined"){
			if(typeof data[key] != "undefined"){
				return data[key];
				}
			else{
				var newVal  = elm.find( this.keys.dataKey + " ." + key);
				if(newVal.length){
					this.dataFromDom(newVal,data);
					elm.data(this.keys.dataKey,data);
					return data[key];
					}
				else return false;
				}
			}
		else {
			
			data[key] = value;
			elm.data(this.keys.dataKey,data);
			var container = elm.find( this.keys.dataKey);
			if(container.length && container.find("."+key).length)container.find("."+key).text(value);
			else container.append("<div class='"+key+"'>"+ value + "</div>");
			return true;
			}
			
		};
	this.getIdName = function(elm){
		var dataObj = this.getDataObj(elm);
		return  {obj : dataObj,idName : this.getDataFromDataObj(dataObj, "EA-id")};
		};
	this.getId = function(elm){
		
		var allData = this.getIdName(elm);
		
		return this.getDataFromDataObj(allData.obj,allData.idName);	
		}
	
	this.init();
	}
EA_menu_panel = function(selector){
	this.dataController = new EA_data_store_obj($(selector),this);
	
	EA_editObj.call(this,"menu","easy_admin");
	this.showAjaxResponse = function(data){}
	this.mainContainer = $(selector);
	var menus = EA_global_helpers.get_quick_menus();
	
	this.menuObj = this.mainContainer.attr('id');
	
	this.menuObj  = this.menuObj.replace('EA-menu-edit-','');
	
	
	for(var x = 0; x<menus.length;x++){
		if(menus[x].menuName == this.menuObj)this.menuObj = menus[x];
		}
	//console.log(this.menuObj);
	var mainContainer = this.mainContainer,
		curObj = this,
		controller = this.dataController;
	//this.elements.ajaxObj.url = 'easy_admin/ajax_panel';
	this.elements.ajaxObj.data.module = 'easy_admin';
	this.elements.ajaxObj.data.objType = 'panel';
	this.selectors.sortableParent = selector + ' .EA-menus-parts-all';
	this.elements.sortableOptions = { 
	//	tabSize: 50,
		tolerance: "pointer",
		toleranceElement: ".EA-no-ul-wrp",
		listType:"ul",
		//handle:".EA-handle:first",
		items:"li",
		placeholder: 'EA-panel-placeholder',
		/*helper: function(event,elm){
			
			//var helper = $('.EA-menu-part:first').clone().addClass('EA-panel-helper').find('ul,.EA-menu-part-extra').remove().end().find('.EA-menu-part-menu-data-wrp').text(Drupal.t('@sum items',{'@sum': (elm.find("li").length + 1)})).end();
			var helper = "<div class='ds'>" + Drupal.t('@sum items',{'@sum': (elm.find("li").length + 1)}) + "</div>";
			return helper;
			},*/
		stop:function(){
			//console.log(mainContainer)
			controller.markAs("sorted",mainContainer);
			mainContainer.find("ul").not(":has('li')").remove();
			mainContainer.find(".EA-menu-part").each(function(){
				var margin = ($(this).next().length ? "" :"0px" );
				$(this).css("margin-bottom",margin);
				if($(this).find("ul").not(":empty").length)
					$(this).find(".EA-plus:first").show()
				else $(this).find(".EA-plus:first").hide()
				});
				
			}
		}
	var findMenuName = selector.replace('#EA-menu-edit-','');
	for(var i =0; i < Drupal.settings.easy_admin.menus.length; i++){
		if(Drupal.settings.easy_admin.menus[i].menuName == findMenuName){
			findMenuName = Drupal.settings.easy_admin.menus[i];
			}
			
		}
	this.elements.sortableOptions.maxLevels = findMenuName.max_depth;
	this.elements.sortableOptions.rtl = Drupal.settings.easy_admin.dir == '1';
	this.elements.sortableOptions.handle = '.EA-handle';
	this.mainContainer.on("click",".EA-plus",function(){
			var ul = $(this).closest('.EA-no-ul-wrp').siblings("ul");
			if(ul.is(":hidden")){
				$(this).addClass("showing").removeClass("hiding");
				ul.show(600);
				}
			else{
				$(this).addClass("hiding").removeClass("showing");
				ul.hide(600);
				}
			});
	this.loadCallback = function(){
		
		$(this.selectors.sortableParent).nestedSortable(this.elements.sortableOptions);
		};
	
	}

$(function(){
	
	if(typeof EAPanelResponseFunc == 'undefined')
		EAPanelResponseFunc = {};
	EAPanelResponseFunc['easy_admin'] = EA_panel.panelShowlResponse;
	EA_data_store = {};
	var first = $('#EA-menu-edit-all > div:first'),
		menuName = first.attr('id').replace('EA-menu-edit-','');
	EA_data_store[menuName] = new EA_menu_panel("#EA-menu-edit-" + menuName);
	EA_data_store[menuName].init();
	$("#EA-menu-select").change(function(){ 
			var menu_name = $(this).val();
			
			if($("#EA-menu-edit-" + menu_name).length){
				$(".EA-menu-edit").hide();
				$("#EA-menu-edit-" + menu_name).show();
				}
			else{
				$.post("/easy_admin/custom_menus/" + menu_name +"/ajax",function(data){
				if(data){
					
					$("#EA-menu-edit-all").append(data);
					$(".EA-menu-edit").hide();
					$("#EA-menu-edit-" + menu_name).show();
					EA_data_store[menu_name] = new EA_menu_panel("#EA-menu-edit-" + menu_name);
					EA_data_store[menu_name].init();
					}
				});
				}
			});
	//EA_data_store.init();
	$('.EA-data-wrp').dblclick(function(e){
		//console.log($(this).data('EA-data'));
		e.stopPropagation();
		});
	$('.EA-save-all').click(function(){
			EA_panel.getAllData(this);
			return false;
			});
	$('.EA-add-new').click(function(){
			/*var ajaxWrp = false
			for(var x in EA_data_store){
				ajaxWrp = EA_data_store[x];
				}*/
			var thisMenu = $('.EA-menu-edit:visible:first'),
				id = thisMenu.attr('id').replace('EA-menu-edit-',''),
				item = EA_data_store[id];
			if(thisMenu)
				EA_panel.addNewItem(thisMenu,item.dataController,false);
			return false;
			});
	});






})(EAJQ);
