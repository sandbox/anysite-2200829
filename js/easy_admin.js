//$ helpers

(function($){
	if(typeof String.prototype.trim){
		String.prototype.trim = function(){
			return this.replace(/\s$|^\s/g,"");	
			}
		} 

		
	easyAdminLinkBrowser = {
		selectors : {
			editBoxSelector : "#EA-edit-box",
			editBoxFormSelector : "#ea-edit-menu-item-form"
			},
		elements : {
			
			},
		flags : {
			inited : false
			},
		mainSubmit : function(){
				var data = {
					objType: "menu",
					action: "save",
					menus : [$(this).mySerializeForm("link_browser")]
					};
				$(this.elements.editBoxFormSelector +" [name=form_id],[name=form_token],[name=form_build_id]").each(function(){
						data[$(this).attr("name")] = $(this).val();
						});
				EA_editObj.ajaxWrapper({url:"easy_admin/ajax_all",data:data});
				easyAdminLinkBrowser.elements.editBoxObj.dialog("close");
				return false;
			},
		init : function(){
			if(this.flags.inited) return;
			

			this.elements.editBoxObj = $(this.selectors.editBoxSelector);
			this.elements.editBoxFormObj = $(this.selectors.editBoxFormSelector);
			//this.elements.editBoxObj.find("form").bind("submit",this.helpers.editBoxFormItemSubmit);
			$("#EA-views-container-inner").on("click keydown",".EA-view a",this.fillInputsBylink);
			$("#EA-views-container-inner").on("click keydown","#EA-add-new-node a",this.nodeAppendIframe);
			//this.elements.editForm.submit(this.mainSubmit);
			$("#EA-edit-box").dialog({
				width:500, 
				//position : { my: "top",  at: "bottom", of: "#content" },
				dialogClass: "up_up_up",
				autoOpen : false
				});
			this.flags.inited = true;
			},
		load : function(data,conf){
			if(!this.flags.inited){
				this.init();
				}
			var linkBrowser = this;	
			var defaultConf = {
				beforeSubmit : function(){},
				submit : function(e){
					var instanceWrpper = new EA_editObj();
					//console.log("ssss")
					e.preventDefault();
					data = {action:"save","type":"menu"};
					data['menus'] = [$(this).mySerializeForm()];
					instanceWrpper.ajaxWrapper({
						data : data,
						success : linkBrowser.conf.success
						});
					return false;
					},
				success : function(data){
					linkBrowser.conf.successHook(data);
					//console.log(data);
					},
				successHook : function(){
					
					}
				};
			this.conf = $.extend({},defaultConf);
			this.conf =  $.extend(defaultConf,conf);
			//console.log(this.conf.submit)
			//console.log(conf)
			
			this.elements.editBoxFormObj.unbind().bind("submit", this.conf.submit);
			
			this.fillForm(data);
			$(this.selectors.editBoxSelector).dialog("open")
			},
		nodeAppendIframe : function(e){
			e.preventDefault();
			var link = $(this).attr("href");
			var curIframe = new EA_global_helpers.iframe(link,function(elm,obj){
				//console.log(curUrl,elm);
				//return 'ggggg';
				var inDoc = elm.contents();
				var curUrl = inDoc[0].location.pathname;
				//console.log(inDoc.find(':input.error').length);
			//	//console.log(curUrl == elm.data("firstUrl").replace(/[?|&]context=narrow/))
			//	//console.log(/^\/node\/add\//.test(curUrl) );
				if(curUrl == elm.data("firstUrl") || /\/node\/add\//.test(curUrl) ){
					//console.log('Still here..');//successfull node form submit should lead to new page. if not, it's validation error. 	
					return;
					} 	
				var title = elm.contents().find("h1").text() ||  elm.contents().find("h2:first").text() || "No title";
				var data = {
					title : title.replace(/^\s+|\s+$/g,""),
					link_path : curUrl.replace(/^\/|\/$/,"")
					};
				easyAdminLinkBrowser.fillInput(data);	
				$("[name=link_title]").val(title.replace(/^\s+|\s+$/g,""));
				$("[name=link_path]").val(curUrl.replace(/^\/|\/$/,""));
				obj.close();
				},{
					title : Drupal.t('Add new page')
					});
			curIframe.open();		
			},
		clearForm : function(){
			$("[name=link_browser_menu_name],[name=link_browser_link_title],[name=link_browser_link_path]").val("");
			$("[name=link_browser_mlid]").val(0);
			},
		fillForm : function(data){
			/*if(typeof data.mlid == "undefined"){
				//console.error(Drupal.t("Reuqired mlid missed"));
				return false;
				}*/
			data = $.extend({title:"",link_path:"",menu_name:""},data);
			$("[name=link_browser_menu_name]").val(data.menu_name);
			$("[name=link_browser_mlid]").val(data.mlid);
			this.fillInput(data);
			},	
		fillInputsBylink : function(){
			var data = {
				title : $(this).text().replace(/^[\s|   ]+|[\s|   ]+$/g,""),
				link_path :decodeURIComponent($(this).attr("href"))
				};
			easyAdminLinkBrowser.fillInput(data);
			$('body').scrollTop($('#ea-edit-menu-item-form').offset().top-10);
			return false;
			},
		 fillInput : function(data){
			 var data = $.extend({title:false,link_path:false},data);
			 var regEx = new RegExp("^https?:\/\/" + location.host +"\/|^\/","g");
			 var regEx2 = new RegExp("^" + easyAdminAjaxPrefix.replace(/\//g,"\\/?").replace(/\?/,"?"),"g");
			 
			 data.link_path = data.link_path.replace(regEx,"").replace(regEx2,"")
			 if(!data.link_path)data.link_path = "<front>";
			 //console.log(regEx2);
			 if(data.title)
				$("[name=link_browser_link_title]").val(data.title);
			 if(data.link_path)
				$("[name=link_browser_link_path]").val(data.link_path);
			 },
		close : function(){
			this.elements.editBoxObj.dialog("close");
			}
		}
	
		
	easyAdminPanel = {
		init : function (){
			this.flags = {always_open:false,opened:false};
			this.printFunctions = (typeof easyAdminPanelTemp !="undefined" ? easyAdminPanelTemp : {});
			this.container = $("#EA-panel");
			this.text = $(".EA-screen-text");
			this.arrow = $(".EA-panel-arrow");
			this.animateDir = (Drupal.settings.dir == "0") ? "right" : "left";
		//	//console.log(this.arrow)
			this.arrow.click(function(){
				easyAdminPanel.toggle();
				});
			this.close(1);
			this.text.on("click",".EA-title",function(){
				$(this).siblings().toggle();
				});
			},
		open : function(speed){
			speed = speed || 600;
			easyAdminPanel.flags.opened = true;
			var animateObj = {};
			animateObj[easyAdminPanel.animateDir] = "10px";
			//console.log(animateObj);
			easyAdminPanel.container.animate(animateObj,speed,function(){easyAdminPanel.arrow.addClass("open");});
			},
		close : function(speed){
			speed = speed || 600;
			easyAdminPanel.flags.opened = false;
			var animateObj = {};
			animateObj[easyAdminPanel.animateDir] = "-239px";
			//console.log(animateObj);
			easyAdminPanel.container.animate(animateObj,speed,function(){easyAdminPanel.arrow.removeClass("open");});
			},
		toggle : function(){
			if(this.flags.opened)this.close();
			else this.open();
			},
		print : function(text){
			this.text.prepend(text);
			this.open();
			if(!this.flags.always_open)setTimeout(easyAdminPanel.close,4000);
			}
		}
	Drupal.theme.prototype.EA_panel_message = function(message,messageType,details){
		var details = details || false;
		var messageType = messageType || "message";
		var html = "<div class='EA-text-part EA-"+ messageType +"'>";
		html += "<div class='EA-title'>" + message + "</div>";
		if(details)
			html += "<div class='EA-description'>" + details + "</div>";
		html += "</div>";
		return html;
		}
	
	$.fn.findDepth = function(el,count){
		count++;
		if($(this).find(el).length)
			return $(this).find(el).findDepth(el,count);
		else return count;
		} 
		
	$.fn.findMlid = function(){
	
		var liClass = $(this).attr("class");
		var regEx =/\smlid-\d+|menu-\d+|EA-menu-part-\d+/;
		liClass = regEx.exec(liClass);
		if(liClass)//console.log(liClass[0]);
		return (liClass? liClass[0].replace(/\D/g,"") : "none");
		} 
	$.fn.mySerializeForm = function(prefix,empty){
		var allMe = {};
		if(prefix){
			var regEx = new RegExp("^" + prefix + "_");
			}
		else regEx =false;
		$(this).find(":input:not('[type=submit]')").each(function(){
			var name = $(this).attr("name");
			if(!name)return;
			if(regEx) name = name.replace(regEx,"");
			if(name=="form_build_id" || name=="form_id"|| name=="form_token"|| name=="op")return;
			name =name.replace(/\]$/,"");
			name =name.replace(/\]\[/,"[");
			name = name.split("[");
			//var val = ($(this).is(":checkbox") ? Number($(this).is(":checked")) : $(this).val());
			var val = ($(this).is(":checkbox") ? ($(this).is(":checked") ? $(this).val() : 0) : $(this).val());
			name.push(val);
			MergeRecursive(allMe,shiftAndshift(name))
			});
		return allMe;
		}
		
	function MergeRecursive(obj1, obj2) {
		  for (var p in obj2) {
			try {
			  // Property in destination object set; update its value.
			  if ( obj2[p].constructor==Object ) {
				obj1[p] = MergeRecursive(obj1[p], obj2[p]);
			  } else {
				obj1[p] = obj2[p];
			  }
			} catch(e) {
			  // Property in destination object not set; create it and set its value.
			  obj1[p] = obj2[p];
			}
		  }
		  return obj1;
		}
	function shiftAndshift(arr){ 
		if(typeof arr=="string")return arr;
		else if(arr.length==1)return arr.shift();
		else{
			var obj = {};
			obj[arr.shift()] = shiftAndshift(arr);
			return obj;
			}
	}
	
	
EA_global_helpers = {
	elements : {
		/*panel : false,*/
		editBoxObj : false,
		confirm : "<div id='EA-confirm'><h3></h3><div class='EA-confirm_text' > </div></div>"
		},
	defaults : {
		yes : {text:Drupal.t("Yes"),name:"yes"},
		no : {text:Drupal.t("No"),name:"no",click:function(){$(this).dialog("close")}}
		},
	confirm : function(text,yes,no){//all variables are objects
		if(!$("#EA-confirm").length) $("body").append(EA_global_helpers.elements.confirm);
		var confirm = $("#EA-confirm");
		var title = text.title || "";
		var text = text.text || "";
		var yes = yes || {};
		var no = no || {};
		var yes = $.extend(this.defaults.yes,yes);
		var oldClick = yes.click;
		yes.click = function(){
			oldClick();
			$(this).dialog("close");			
			};
		var no = $.extend(this.defaults.no,no);
		
		var buttons = [yes,no];
		//console.log(buttons);
		confirm.find("h3").html(title).end().find(".EA-confirm_text").html(text).end().dialog({buttons:buttons});
		},
	get_quick_menus : function (){
			var menus = Drupal.settings.easy_admin.menus;
			
			menuArr =[];
			for(x in menus){
				if(!menus[x].htmlID)
				//console.log(x);
					switch(menus[x].menu_name){
						case "main-menu":
							if($("#main-menu").length)
								menus[x].htmlID = "#main-menu";
						case "management":
						case "navigation":
						case "user-menu":
							if($("#block-system-" + x ).length)
								menus[x].htmlID = "#block-system-" + x;
						default:
							if($("#block-menu-" + x ).length)
								menus[x].htmlID = "#block-menu-" + x;
						}
				menus[x].menuName = menus[x].menu_name;
				menus[x].uniqueID = x;
				var converArr = Array('is_enable','is_replaceable','max_depth');
				for(var i =0; i < converArr.length;i++){
					menus[x][converArr[i]] = parseInt(menus[x][converArr[i]]);
					}
				menuArr.push(menus[x]);
				}
			//console.log(menuArr);
			return menuArr;
			},
		iframe : function(link,callback,dialog){
			var date = new Date(),
				dialog = dialog || false;
			this.id = "EA-iframe-" + date.getTime();
			this.link = link;
			this.link += (this.link.indexOf("?")>-1 ? "&" : "?") + "context=narrow";
			this.callback = callback;
			this.dialog =  {
				
				open : function( event, ui ){
					//console.log($(this));
					if(dialog && typeof dialog.openCallback == 'function')diaolg.openCallback( event, ui );
					var cssObj = {
							position : 'fixed',
							height : $(window).height(),
							top:0,
							},
						direction = Drupal.settings.easy_admin.dir  == '1' ? "left" : "right";
						cssObj[direction] = 0;
					if(direction != 'left')
						cssObj['left'] = 'inherit';
					//console.log(cssObj);
					$(this).closest('.ui-dialog').css(cssObj).end().css('height','100%').find('iframe').css('height','93%');
					}
				};
			this.dialog =  $.extend(true,this.dialog ,dialog);
			var curIframeObj = this;
			this.open = function(){
				var myId = this.id;
				var myIframe = "<div id='" + myId + "' class='EA-iframe EA-node-add'><iframe id='" + myId + "-iframe' name='" + myId + "-iframe' src='" + this.link + "'></iframe></div>";
				$("body").append(myIframe);
				$("#" + myId + "-iframe").load(function(){
					var curIframe = $("#" + myId + "-iframe")
					curIframe.off('load');
					curIframe.data("firstUrl",$(this).attr("src"));			
					var inDoc = $(this).contents();
					curIframe.parent().dialog(curIframeObj.dialog);		
					var body = inDoc.find('body');
					//console.log($(this).closest('.ui-dialog'),body);
					var dialog = $(this).closest('.ui-dialog');
					curIframe.width(dialog.width());
					curIframe.closest('.ui-dialog').resizable('option','alsoResize',$(this));
					if(curIframeObj.callback)
						curIframe.load(function(){
							
							var curLocation = $(this).contents()[0].location;
							//console.log(curIframeObj.link,(curLocation.pathname  + curLocation.search))
							if(curIframeObj.link != (curLocation.pathname  + curLocation.search))
								curIframeObj.callback($(this),curIframeObj);
							});
					});
				},
			this.close = function(){
				$("#" + this.id).empty().dialog("destroy");
				$("#" + this.id).remove();
				}
			}
		} 
			



	EA_editObj = function(objType,moduleName){
	
		this.objType = objType;
		this.moduleName = moduleName;
		this.additionalData = {};
		this.flags = {sortable:false,popUp:true};
		var editObjCur = this;
	//	//console.log(editObjCur)
		this.selectors ={
			dataContainer : ".EA-hidden",
			mainContainer : false,
			objPartHtml : false,
			sortableParent : false,
			sort: "li",
			};
		this.elements= {
			ajaxObj : {
				url:"easy_admin/ajax_all",
				type:"POST",
				error:function(){					
					var message =  Drupal.theme("EA_panel_message",Drupal.t("Operation failed"),"error");
					easyAdminPanel.print(message);
					},
				success: function(data){
					//console.log(editObjCur.menuName)
					editObjCur.showAjaxResponseWrp(data);
					},
				dataType:"json",
				data : {
					nodeType :this.nodeType,
					moduleName :this.moduleName,
					
					}
				},
			editForm : false,
			sortableOptions : {
					connectWith : this.selectors.sortableParent,
					update : function(event, ui){
					//	//console.log(ui.item.closest(this.selectors.sortableParent));
						ui.item.closest(editObjCur.selectors.sortableParent).addClass("changed")},
					out : function(event, ui){
						//console.log(ui.item.closest(this.selectors.sortableParent));
						ui.item.closest(editObjCur.selectors.sortableParent).addClass("changed");
						}
					}
			};
		
		this.mainContextMenu = function(key,opt,editItem){
			//console.log(key,opt,editItem)
			var objFunction = editItem[key];
			objFunction(opt.$trigger,opt,editItem);
			};
		this.ajaxWrapper = function(data){
			if(data.successCallback){
				data.success = function(d){
					data.successCallback(d);
					editObjCur.showAjaxResponseWrp(d);
					}
				}
			//console.log(data);
			var newData = $.extend(true,{},this.elements.ajaxObj);
			//console.log(newData);
			var data = $.extend(true,newData,data);
			if(editObjCur.elements.editForm){
				editObjCur.elements.editForm.find("[name=form_id],[name=form_token],[name=form_build_id]").each(function(){
					//console.log(data.data)
					data.data[$(this).attr("name")] = $(this).val();
					});
				}
			data.url = easyAdminAjaxPrefix + data.url;
			$.ajax(data);
			};	
		this.showAjaxResponseWrp = function(data){
			if(data.STATUS == "ERROR"){
				var message =  Drupal.theme("EA_panel_message",Drupal.t("Operation failed"),"error",data.TEXT);
				easyAdminPanel.print(message);
				}
		
			if(!data )return;
			//console.log(data);
			if(typeof data.panel_messages != "undefined" && data.panel_messages.length)
				for(var x =0; x<data.panel_messages.length;  x++){
					var thisMsg = data.panel_messages[x];
					if(!thisMsg.type)thisMsg.type = 'message';
					var message = Drupal.theme("EA_panel_message",thisMsg.title , thisMsg.type , thisMsg.description  );
					easyAdminPanel.print(message);
					}
			//console.log(this)
			this.showAjaxResponse(data);
			this.afterAjax(data);
			if(this.elements.editForm && this.flags.popUp)
				this.elements.editForm.dialog("close");
			return;
			};
		this.showAjaxResponse = function(data){},
		this.afterAjax = function(){};
		this.saveOrder = function(key,opt,object){
			var parts = {};
			//console.log(object)
			//var object= this;
			$(object.selectors.sort,object.mainContainer).each(function(index){
				if(object.findId($(this)))
					parts[object.findId($(this))] = index;
				})
			var data = {objType : object.objType ,action:"order"};
			data[ object.objType ]= parts;
			//console.log(object)
			object.ajaxWrapper({data:data}); 
			};
		this.delete_warn = function(obj){
			var text = {
				title:Drupal.t("Warning"),
				text:Drupal.t("You are going to delete @objType.",{ '@objType': Drupal.t(editObjCur.objType)}) + "<br/>" +Drupal.t("this action cannot roll back!") };
			var yes = {click:function(){editObjCur.deleteItem(obj)}};
			EA_global_helpers.confirm(text,yes);
			}
		this.deleteItem = function(obj){
			//console.log(editObjCur)
			if(editObjCur.findId(obj))
				var data = {
						action : "delete",
						module : editObjCur.moduleName,
						objType : editObjCur.objType
						}
				
				data[ editObjCur.objType ]= Array(editObjCur.findId(obj));
				editObjCur.ajaxWrapper({
					data :data
					}); 
			};
		this.edit = function(obj,opt,item){
			editObjCur.copyToForm(obj);
			if(editObjCur.flags.popUp)
				editObjCur.elements.editForm.dialog("open");//.css({position:"absolute"/*,top:e.clienty,left:client*/});
			};
		this.add = function(){
			editObjCur.copyToForm(false);
			if(editObjCur.flags.popUp)
				editObjCur.elements.editForm.dialog("open");
			};
		this.save = function(){
			
			if(!this.save_validate()) return;
			var data = this.dataFromForm();
			this.ajaxWrapper({
				data : data,
				action:"save",
				objType:this.objType,
				module : this.moduleName
//				successCallback : this.saveSuccessCallback
				}); 
				
			};
		this.dataFromForm = function(prefix){
			if (typeof prefix=="undefined" ) prefix = false ;
			return this.elements.editForm.mySerializeForm(prefix);
			}
		this.save_validate= function(){
			return true;				
			};
		this.findId = function(domElm){return false	};
		this.editForm = function(){
			var data = $(this).mySerializeForm();
			data.action = "save";
			data.module = editObjCur.moduleName;
			data.type = editObjCur.objType;
			data.objType = editObjCur.objType;
			if(typeof CKEDITOR!="undefined")
				var myInstances  = CKEDITOR.instances;
			else var myInstances  =[];
			for(var x in myInstances){
				var keys = x.replace("edit-","").split("-");
				var pointer = data,counter= 0;
				while(counter < keys.length){
					if(typeof pointer[keys[counter]] == "undefined")
						pointer[keys[counter]] = {};
					pointer = pointer[keys[counter]]; 
					counter++; 
					}
				pointer = myInstances[x];
				}
			//console.log(data);
			editObjCur.ajaxWrapper({data:data})
			return false;
			};
		this.copyToForm =function(opt){
			var thereIsCkeditor = typeof CKEDITOR!="undefined";
			var form = this.elements.editForm;
			curElm = this;
			var formData = [];
			
			if(!opt){
				form.find(":input").not(":submit").each(function(){
					var name = $(this).attr("name");
					if($.inArray(name,["form_build_id","form_token","form_id"]) != -1)return;
					if(name)
						{
						newName = name.replace(/\]|\[|_/g,"-").replace(/-{2,}|-$/g,"-").replace(/-$/,"");
						if(thereIsCkeditor && $(this).is("textarea:hidden")){
							//console.log('edit-en-' + formData[x].newName)
							//console.log(CKEDITOR.instances['edit-en-' + formData[x].newName])
							if(typeof CKEDITOR.instances['edit-en-' + newName] != "undefined")
								CKEDITOR.instances['edit-en-' + newName].setData("");
								return;
								}
					
						$(this).val("")
						}
					});
				curElm.callACallback("loadData",form,false);
				return;
				}
			var dataContainer = opt.find(this.selectors.dataContainer);
			
			form.find(":input").not(":submit").each(function(){
				var name = $(this).attr("name");
				if($.inArray(name,["form_build_id","form_token","form_id"]) != -1)return;
				if(name)
					{
					newName = name.replace(/\]|\[|_/g,"-").replace(/-{2,}|-$/g,"-").replace(/-$/,"");
					formData.push({name:name,newName:newName});
					}
				});
			
			
			for(var x =0;x<formData.length; x++){
				//console.log(formData[x].name);
				//console.log(formData[x].newName);
				var curData = opt.find( this.selectors.dataContainer + " ." + formData[x].newName)
				var curInput = form.find("[name='" + formData[x].name + "']");
				//console.log(curData.html(),curInput)
				if(!curData.length)continue;
				
				if(curInput.is("textarea.ckeditor-processed") && thereIsCkeditor){
					//console.log('edit-en-' + formData[x].newName)
					//console.log(CKEDITOR.instances['edit-en-' + formData[x].newName])
					if(typeof CKEDITOR.instances['edit-' + formData[x].newName] != "undefined")
						//console.log(CKEDITOR.instances['edit-' + formData[x].newName])
						//console.log(CKEDITOR.instances['edit-' + formData[x].newName])
						CKEDITOR.instances['edit-' + formData[x].newName].setData(curData.html())
					}
				else
					curInput.val( curData.text())
				}
			editObjCur.callACallback("loadData",formData,opt.find( this.selectors.dataContainer));
			};
		this.init = function(){//called onces, on start
			var obj = this;
		//	//console.log(this.menuName)
			this.menuChildrenItems =  {
				choose : {
					name : " - Choose option -",
					callback:function(){},
					icon : "none"
					},
				delete_warn : {
					name : Drupal.t("Delete @objType",{ '@objType': Drupal.t(editObjCur.objType)}),
					icon: "delete",
					callback: function(key,opt){
						obj.mainContextMenu(key,opt,obj)
						}
					},
				edit : {
					name : Drupal.t("Edit @objType",{ '@objType': Drupal.t(editObjCur.objType)}),
					icon: "edit",
					callback: function(key,opt){
						obj.mainContextMenu(key,opt,obj);
						}
					},
				add : {
					name : Drupal.t("Add new @objType",{ '@objType': Drupal.t(editObjCur.objType)}),
					icon: "add",
					callback: function(key,opt){
						obj.mainContextMenu(key,opt,obj);
						}
					}
				}
			if(this.elements.editForm.length && this.flags.popUp)
				editObjCur.elements.editForm.dialog({
					width:700,
					autoOpen:false					
					}).submit(this.editForm)
			this.callACallback("init");
			this.load();
			
			},
		this.load = function(){//called each time content loaded
			var curElm = this;//to remeber the current object in build function
			
			var selector = "";
			
			if(this.selectors.objPartHtml && this.selectors.objPartHtml.indexOf(",")!=-1)
				selector =  this.selectors.objPartHtml.replace(/,/g, "  " +  this.selectors.sort + "," + this.selectors.mainContainer + " " );
			selector =  this.selectors.mainContainer  +" " + selector + " " + this.selectors.sort ;
			
			$.contextMenu({
				zIndex:10,
				selector : selector,
				build :function($trigger){	
					//console.log(this)		
					var returnItems  = $.extend({},curElm.menuChildrenItems);
					//console.log(curElm)
					if(curElm.flags.sortable && $trigger.closest(curElm.selectors.sortableParent),$trigger.closest(curElm.selectors.sortableParent).is(".changed"))
						returnItems['saveOrder'] = {
							name : Drupal.t("Save order"),
							icon: "save",
							callback: function(key,opt){
								curElm.mainContextMenu(key,opt,curElm);
								}
							};
					return {items:returnItems}; 
					}
				});
			
			if(this.flags.sortable && $(this.selectors.sortableParent).length){
				
				$(this.selectors.sortableParent).sortable(this.elements.sortableOptions);
				
				}
			this.callACallback("load");
			};
		this.callACallback = function(name){
			var args = Array.prototype.slice.call(arguments);
			name = args.shift();
			//console.log(name,args);
			var func = this[name + 'Callback'];
			if(typeof func == "function"){
				func.apply(this,args);
				}
			}
	

		}
EA_edit_menu = function(obj){
	var wrpClass = obj.htmlID;
	if(typeof wrpClass == "undefined" || !$(wrpClass).length){
		//console.error("Menu init error! Undefined htmlID or menuName. Details for object in next error message");
		//console.error(obj);
		return;
		}
	EA_editObj.call(this,"menu","easy_admin");
	var obj = obj || {};
	
	this.selectors.mainContainer = wrpClass || false;
	this.menuName = obj.menuName || false;
	this.menuTitle = obj.menuTitle || false;
	this.mainContainer = $(wrpClass);
	this.selectors.sort = 'li';
	this.elements.editForm = $("#ea-edit-menu-item-form");
	this.flags.popUp = false;
	this.obj  = obj;
	
	//console.log(obj,this.obj)
	this.findId = function(domElm){
		var mlid = domElm.data("EA_mlid")
		if(mlid)return mlid;

		var liClass = domElm.attr("class");
		var regEx =/\smlid-\d+|menu-\d+|EA-menu-part-\d+/;
		liClass = regEx.exec(liClass);
		//if(liClass)//console.log(liClass[0]);
		mlid =  (liClass? liClass[0].replace(/\D/g,"") : "none");
		domElm.data("EA_mlid",mlid);
		return mlid;
		}
	//this.elements.editForm = $("#EA-edit-menu-item-form");
	this.flags.sortable = true;
	this.selectors.sortableParent  = "ul.links,ul.menu,ul.nice-menu";// "ul.links,ul.menu,ul.nice-menu,ul.links ul,ul.menu ul,ul.nice-menu ul";
	this.elements.ajaxObj.successCallBack = function(){
		
		};
	this.initCallback = function(){
		var curObj = this;
		this.mainContainer.addClass('EA-menu-edit-' + this.obj.uniqueID);
		this.mainContainer.find(this.selectors.sortableParent).filter(".menu,.links").find("li").addClass("EA-big").append("<div class='EA-handle'></div>");
		if(this.obj.is_enable > 1)
			this.menuChildrenItems['advacedEdit'] = {
				name : Drupal.t("Advaced Edit"),
				icon: "edit",
				callback: function(key,opt){
					curObj.mainContextMenu(key,opt,curObj);
					}
				}
		}
	this.selectors.objPartHtml = "ul.links,ul.menu,ul.nice-menu";
	var menuObj = this;
	this.advacedEdit = function(){
		//console.log(menuObj.menuName);return;
		window.open(easyAdminAjaxPrefix+ 'admin/structure/menu/manage/' + menuObj.menuName + '/easy_admin','_blanck')
		};
	this.edit = function(obj,opt,item){
		var link =  obj.find("a:first");
		var data = {
			link_path : decodeURI(link.attr("href").replace(Drupal.settings.easy_admin.easyAdminAjaxPrefix,"").replace(/^\//,"")),
			title : link.text(),
			mlid : item.findId(obj),
			menu_name : item.menuName
			};
		//var menuObj = item;
		easyAdminLinkBrowser.load(data,{submit : function(){
			//console.log(item.elements)
			var data = menuObj.dataFromForm("link_browser");
			data = {
				menu : [data],
				action : "save",
				objType :item.objType,
				moduleName :item.moduleName
				}
			//console.log(data)
			item.ajaxWrapper({data:data});
			return false;
			},success:function(d){
				//console.log(d)
				}
				});
		};
	this.add = function(obj,opt,item){
		easyAdminLinkBrowser.clearForm();
		easyAdminLinkBrowser.load({mlid:0,menu_name:item.menuName},{
			submit:function(){
				item.save();
				//console.log(item.save)
				return false;
				}
			});
		}
	this.save = function(){
		//console.log(this)
		if(!this.save_validate()) return;
		var data = {menu : [this.dataFromForm("link_browser")]};
		data['objType'] = this.objType;
		data['action'] = "save";
		//console.log(data)
		this.ajaxWrapper({
			data : data,
//				successCallback : this.saveSuccessCallback
			}); 
			
		};
	//console.log(this)
	this.showAjaxResponse = function (data){
		for(var operation in data.operations){
			var oper = data.operations[operation];
			//console.log(this.obj.is_replaceable,this.mainContainer.find("ul:not('.contextual-links'):first"))
			if(operation == "replace_all"){
				//console.log(this.obj);
				if(parseInt(this.obj.is_replaceable)){
					var html = $(oper);
					if(parseInt(this.obj.is_replaceable) == 2){//this id 'flat' replace
						//console.log(html);
						html.find('ul').remove();
						}
					//console.log(html);
					this.mainContainer.find("ul:not('.contextual-links'):first").html(html);
					this.load();
					}
				else
					EA_global_helpers.confirm({
						title:Drupal.t("Refresh page?"),
						text:Drupal.t("Menu had changed, but changes would'nt appear until refreshing the page. Refresh now?")
						},
					{
						click:function(){location.href = location.href}
						},
					{
					text:Drupal.t("Not now")
					});
				continue;
				}
			if(typeof oper.action =="undefined" )continue;
			
			
			$(this.selectors.sortableParent,this.mainContainer)[oper.action](oper.class);
			}
		
		easyAdminLinkBrowser.close();
		};
	this.init();
	}


$(function(){
		//easy_admin.init();
		easyAdminAjaxPrefix = Drupal.settings.easy_admin.ajaxPrefix;
		easyAdminPanel.init();
		easyAdminLinkBrowser.init();
		//console.log(Drupal.settings);
		Drupal.behaviors.easy_admin_helpers =  {
    		attach:function (context){
    			var events = jQuery('#edit-call-views').data('events').change;
    			var oldEvent = false;
    			for(var x = 0; x < events.length; x++){
    				if(events[x].handler.toString().replace(/\n|\s/g,"") == 'function(event){returnajax.eventResponse(this,event);}') //really ugly way to be sure this is the one
    				oldEvent = events[x].handler;
    				}
    			if(oldEvent){
    				//console.log(oldEvent);
    				jQuery('#edit-call-views').unbind('change',oldEvent);
					jQuery('#edit-call-views').bind('change',function(e){
						//console.log('sss')
						var selector = $(this).val().replace(/_|:/g,"-");
						var cont = $("#easy_admin-" + selector + ",#EA-" + selector);
						if(cont.length && cont.html())
							cont.show().siblings().hide();
						else oldEvent(e);
						});
    				}
			
    			}};
    	EA_menus = {};
	var EA_menu_all_menus = EA_global_helpers.get_quick_menus();
	
	for(var x in EA_menu_all_menus){//we need to override same selectors for differnt manus. we prefer the longer selector
		var menu = EA_menu_all_menus[x],
			domEl = $(menu.htmlID)[0];
			for(var i in EA_menu_all_menus){
				if(x==i)continue;
				var innerMenu = EA_menu_all_menus[i],
					innerDomEl = $(innerMenu.htmlID)[0];
				if(innerDomEl == domEl){
					///if(innerMenu.htmlID.length > menu.htmlID.length)delete(EA_menu_all_menus[x]);
				//	else delete(EA_menu_all_menus[i]);
					}
				}
		
		}
	for(var x in EA_menu_all_menus){
		var menu = EA_menu_all_menus[x]
		if(menu.is_enable == 1 || menu.is_enable == 3)
			EA_menus[menu.uniqueID] = new EA_edit_menu(menu)
		}
			
		if(easyAdminAjaxPrefix && easyAdminAjaxPrefix!="/") easyAdminAjaxPrefix += "/";
		if(location.pathname =="/admin/config/easy_admin"){
			$("#EA-menu-form :checkbox").click(function(){
				var display = ($(this).is(":checked") ? "block":"none");
				$(this).parent().next().css("display",display);
				});
			}
		});
})(EAJQ);
		
