<?php


function easy_admin_menu_form($form, &$form_state ) {
  
  $options = array( );
  $form = array( );
  $menu = easy_admin_get_names( );
  //variable_set("easy_admin_menus_quick", array( ) );
  $prev_values = isset($form_state['values']['menus'] ) ? $form_state['values']['menus'] : variable_get("easy_admin_menus_quick", array( ) );
  //dpm($prev_values );
  //if ( !empty($form_state['values']['menus'] ) ) {
  //  $prev_values =$form_state['values']['menus'];
  //  }
  //echo my_print_r($prev_values );
  $menus_num = isset($form_state['values']['menus'] ) ? count($form_state['values']['menus'] ) : count($prev_values );
  $form['menus'] = array(
    "#type" =>   "fieldset", 
    '#tree' =>   TRUE, 
    '#prefix' =>   "<h1>" . t("main settings for easy admin module" ) . "</h1>", 
    );
    
  for($key =0; $key <= ($menus_num );$key ++ ) {
      
    $default_values = (isset($prev_values[$key] ) ?  $prev_values[$key] : array(
      "is_enable" =>   0, 
      "htmlID" =>   "", 
    //  "is_advanced_enable" =>   0, 
      "max_depth" =>   3, 
      'is_replaceable' =>   0, 
      'menu_admin_name' =>   t('the new menu' ), 
      'menu_name' =>   'main_menu', 
      ) );
    //$edit = $default_values['is_enable'];
    //if ( $default_values[''] )
    //$default_values = $prev_values[$key];
    $form['menus'][$key] = array(
      "#type" =>   "fieldset", 
      '#tree' =>   TRUE, 
      'menu_admin_name' =>   array(
        '#type' =>   'textfield', 
        "#title" =>   t("Human name" ) , 
        "#default_value" =>   $default_values['menu_admin_name'] != t('the new menu' ) ? $default_values['menu_admin_name'] :"", 
        "#description" =>   t("Human readable name for administrative" ), 
        //'#required' =>   TRUE, 
        ), 
      'menu_name' =>   array(
        '#type' =>   'select', 
        '#options' =>   $menu, 
        "#default_value" =>   $default_values['menu_name'], 
        ), 
      "is_enable" =>   array(
        "#type" =>   "select", 
        "#default_value" =>   $default_values['is_enable'], 
        '#options' =>   array(
          t('None' ), 
          t("On the fly edit" ), 
          t("Advanced edit" ), 
          t("Both" ), 
          ), 
        "#description" =>   t("Choose if the menu could be edited on the fly, Easy admin panel style or both. Choose 'none' to remove this entry" ), 
        "#title" =>   t("Edit type" ), 
        ), 
      'advanced_config' =>   array(
        '#title' =>   t('Advanced configuration' ), 
        '#type' =>   "fieldset", 
        '#tree' =>   TRUE, 
        '#collapsed' =>   TRUE, 
        '#collapsible' =>   TRUE, 
        "html_id_container" =>   array(
          "#type" =>   "container", 
            "htmlID" =>   array(
              "#type" =>   "textfield", 
              "#default_value" =>   $default_values['htmlID'], 
              "#description" =>   t("Easy admin trys some html basic markup to find the main 'ul' tag of menu. If you make changes in theme, you can try give your selector." )  ."<br/>" . t("The selector should be the ul parent" ), 
              "#title" =>   t('custom selector for menu' ), 
              ), 
            ), 
          "max_depth" =>   array(
            "#type" =>   "select", 
            '#options' =>   range(1, 9 ), 
            "#default_value" =>   (isset($default_values['max_depth'] ) ? --$default_values['max_depth'] :2 ), 
            "#title" =>   t("Max depth for menu" ), 
            "#description" =>   t("Apply to advanced edit panel" ), 
            ), 
          "is_replaceable" =>   array(
            "#type" =>   "select", 
            "#default_value" =>   (isset($default_values['is_replaceable'] ) ) ? $default_values['is_replaceable'] : 0, 
            "#title" =>   t("Enable replace menu on the fly when changing data" ), 
            "#options" =>   array(
              t('None' ), 
              t('Ask for refreshing' ), 
              t('Auto replace (flat )' ), 
              t('Auto replace (full )' ), 
              ), 
            "#description" =>   t("Auto replace Should'nt be enabled with expanded menus or another JS integration. Replace flat needed for example in main links menu" ), 
            ), 
        ), 
      
      
      
    /*  "menuTitle" =>   array(
        "#type" =>   "hidden", 
        "#value" =>   $default_values['menu_name'], 
        ), */
    
      );
    if ( !$default_values['htmlID'] ) $form[$key]['html_id_container']['#attributes'] = array(
      'class' =>   array("easy_admin_hidden" ), 
      );
    }
    $form["add_more"] =array(
      "#type" =>   "button", 
      "#value" =>   t("Add more" ), 
      '#ajax' =>   array(
        'wrapper' =>  'easy-admin-menu-form', 
          'callback' =>   'easy_admin_menu_form_ajax', 
          'method' =>   'replace', 
        ), 
      );
    $form["submit"] =array(
      "#type" =>   "submit", 
      "#value" =>   t("Save" ), 
      
      );
  
  return $form;
  }

function easy_admin_menu_form_validate($form, &$form_state ) {
  foreach ($form_state['values']['menus'] as $key =>   $value ) {
    //dpm($form_state['values'] );
    
    if ( $value['is_enable'] && !$value['menu_admin_name'] ) {    
      form_set_error("menus][$key][menu_admin_name", t('Please add human name for menu' ) );
      }
    }
  }  
function easy_admin_menu_form_submit($form, &$form_state ) {
  $menus = db_query("SELECT menu_name, title FROM {menu_custom}" )->fetchAllKeyed( );  
  foreach ($form_state['values']['menus'] as $key =>   &$value ) {
    $value += $value['advanced_config'];
    unset($value['advanced_config'] );
    $value['menuTitle']   = $menus[$value['menu_name']];
    $value['htmlID'] = $value['html_id_container']['htmlID'];
    unset($value['html_id_container'] );
    $value['max_depth'] ++;//because we used array_fill we need to  fix the value
    if ( !$value['is_enable'] )
      unset($form_state['values']['menus'][$key] );  
    }
  //dpm($form_state['values']['menus'] );
  variable_set("easy_admin_menus_quick", $form_state['values']['menus'] );
  
  }

function easy_admin_menu_form_ajax($form, &$form_state ) {
  //dpm($form_state['values']['menus'] );  
  //easy_admin_menu_form_submit($form, $form_state );
  return $form;
  }

function easy_admin_links_browse( ) { 
  $node_types = node_type_get_types( );
  //$node_types_default = variable_set("easy_admin_chosen_node", array( ) );
  $node_types_default = variable_get("easy_admin_chosen_node", array( ) );
  $form = array( );
  $form['options'] = array(
    '#type' =>   'fieldset', 
    '#tree' =>   TRUE, 
      '#title' =>   t('Choose which options would appear in links\' browser' ), 
     );
  $links_browser = variable_get('easy_admin_links_browser', easy_admin_get_default_links_browser( ) );
  
  
  foreach (easy_admin_get_default_links_browser( ) as $key =>   $value ) {
    $form['options'][$key] = array(
      '#type' =>   'checkbox', 
      '#title' =>   t($value ), 
      '#default_value' =>   isset($links_browser[$key] ), 
      );
    if ( isset($links_browser[$key] ) )unset($links_browser[$key] );
    }
  $d_value = "";
  foreach ($links_browser as $key =>   $value ) {
    $d_value .= "$key|$value";
    }
  $form['options']['all'] = array(
    '#type' =>   'textarea', 
    "#default_value" =>   $d_value, 
    '#attributes' =>   array('style' =>   'direction:ltr;' ), 
    '#description' =>   "<p>" . t('You can add your own views for the links browser (see documention )' ) . "</p>"
             . "<p>" . t('Add new options in new line with the format VIEW_NAME:VIEW_DISPLAY|translateable title' ) . "</p>", 
    
    );
  $form['pages'] = array(
    '#type' =>   'fieldset', 
    '#tree' =>   TRUE, 
      '#title' =>   t('Choose which node types are pages, which can added to menu' ), 
     );
  $form['pages']['title']= array("#markup" =>   "  <p>". t("e.g. : Page or article usually are pages. Part of carualse usually not" )  . "</p>" );
  foreach ($node_types as $key =>   $node_type ) {
    $form['pages'][$key] = array("#type" =>   "checkbox", "#title" =>   $node_type->name );
    $form['pages'][$key]["#default_value"] = isset($node_types_default[$key] ) ? $node_types_default[$key] : 0;
    }
  //dpm($node_types_default );
  
  $form['pages']['add_new'] =  array(
    "#type" =>   "checkbox", 
    '#title' =>   t('apply this for node default view as well' ), 
    '#default_value' =>   variable_get("easy_admin_chosen_node_in_view", 1 ), 
    );
    
  $form['specail_pages'] = array(
    '#type' =>   'fieldset', 
    '#title' =>   t('Special pages' ), 
     '#description' =>   t("Add url which are not genere pages (e.g. not nodes, terms, etc )" ), 
     
     );
  $specail_pages = variable_get('EA_special_pages', array("<front>" =>   "Home", ) );
  $d_value = '';
  if ( is_array($specail_pages ) )
    foreach ($specail_pages as $key =>   $value ) {
      $d_value .= "$key|$value\n"; 
      }
  $form['specail_pages']['EA_special_pages'] = array(
      '#type' =>   'textarea', 
      '#attributes' =>   array('style' =>   'direction:ltr;' ), 
      "#default_value" =>   $d_value, 
      '#description' =>   t('Add new links in new line with the format path|translateable title' ), 
      );   
  $form['submit'] = array("#type" =>   "submit", "#value" =>   t("save" ) );
  return $form;
  }

function easy_admin_links_browse_submit($form, $form_state ) {
  //dpm($form_state['values'] );
  $select_values = easy_admin_get_default_links_browser( );
  foreach ($form_state['values']['options'] as $key =>   $value ) {
    if ( !$value )unset($select_values[$key] );
    }
  $more_values = preg_split("/\n/", $form_state['values']['options']['all'] );
  foreach ($more_values as $value ) {
    $splited_value = explode("|", $value );
    $select_values[$splited_value[0]] = $splited_value [1];
    }
  variable_set('easy_admin_links_browser', $select_values );
  //dpm($select_values );
  variable_set("easy_admin_chosen_node_in_view", $form_state['values']['pages']['add_new'] );
  unset($form_state['values']['pages']['add_new'] );
  variable_set("easy_admin_chosen_node", $form_state['values']['pages'] );
  $values = array( );
  $more_values = preg_split("/\n/", $form_state['values']['EA_special_pages'] );
  foreach ($more_values as $value ) {
    if ( !$value )  continue;
    $splited_value = explode("|", $value );  
    $values[$splited_value[0]] = $splited_value [1];
    }
  variable_set('EA_special_pages', $values );
  drupal_set_message(t("configuration saved" ) );
  }

function easy_admin_prefences( ) {
  $options =  list_themes( );  
  foreach ($options as $key =>   &$value ) {
    $value = $key;
    }
  //dpm($options );
  $form = array(
    'choose_js' =>   array(
      "#type" =>   "checkbox", 
      "#title" =>   t("Load jQuery library from core" ), 
      "#description" =>   t("Check this option just if in your site installed !link and choose jQuery is 1.7 or higher", array("!link" =>   l("jQuery update", "https://drupal.org/project/jquery_update" ) ) ), 
      "#default_value" =>   variable_get("easy_admin_choose_js", 0 ), 
      '#prefix' =>   "<h2>" . t('Choose which jQuery library to load' ) . "</h2>", 
      '#suffix' =>   "<hr/>", 
      ), 
    'choose_narrow_theme' =>   array(
      "#type" =>   "select", 
      "#description" =>   t("Select which theme would be displayed in narrow fashion windows (admin panel )" ), 
      "#title" =>   t("Select narrow theme" ), 
      "#default_value" =>   variable_get("easy_admin_narrow_theme", 'seven' ), 
      '#options' =>   $options, 
      ), 
    'separator_text' =>   array(
      "#type" =>   "textfield", 
      "#description" =>   t("If you want text instead of link logo between link and it's page fill one here" ), 
      "#title" =>   t("Linked to text" ), 
      "#default_value" =>   variable_get("easy_admin_separator_text", 1 ), 
      ), 
    'hide_sidebars_at_panel' =>   array(
      "#type" =>   "checkbox", 
      "#description" =>   t("Check it to hide both sidebars in easy admin panel" ), 
      "#title" =>   t("Hide sidebar in panel" ), 
      "#default_value" =>   variable_get("easy_admin_hide_sidebars", 1 ), 
      ),     
    'submit' =>   array(
      "#type" =>   "submit", 
      "#value" =>   t("Save" ), 
      )
    );
  return $form;
  }

function easy_admin_prefences_submit($form, $form_state ) {
  variable_set("easy_admin_choose_js", $form_state['values']['choose_js'] );
  variable_set("easy_admin_narrow_theme", $form_state['values']['choose_narrow_theme'] );
  variable_set("easy_admin_hide_sidebars", $form_state['values']['hide_sidebars_at_panel'] );
  
  //remeber old linked_to to know if we need to flush theme cache
  $linked_old = variable_get("easy_admin_separator_text", 1 );
  variable_set("easy_admin_separator_text", $form_state['values']['separator_text'] );
  if( $linked_old != $form_state['values']['separator_text'])
      drupal_theme_rebuild();
  }
