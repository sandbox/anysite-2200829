<?php

abstract class EA_ajax_worker{
  public $return = TRUE;
  public function plural($type, $count, $action, $item_type ) {
    //die(print_r(array($type, $count, $action, $item_type ), TRUE ) );  
    if ( $type == 'success' )
      return format_plural($count, ':action of one :item_type succeed', ':action of @count :item_type succeed', array(
        ':item_type' =>   $item_type, 
        ':action' =>   $action, 
        ) );
    elseif ( $type == 'failed' ) {
      return format_plural($count, ':action of one :item_type failed', ':action of @count :item_type failed', array(
        ':item_type' =>   $item_type, 
        ':action' =>   $action, 
        ) );
      }
    }
  public function error($array, $add_data = NULL ) {
    if ( is_numeric($array ) ) {
      switch ($array ) {
        case 2:
          $array = array("CODE" =>   2, "TEXT" =>   t("No data supplied" ) );
          break;
        case 3:
          $array = array("CODE" =>   3, "TEXT" =>   t("Action for type not found" ) );
          break;
        }
      }  
    if ( $add_data ) $array['DATA'] = $add_data;
    $array['STATUS'] = "ERROR";
    //if ( $add_data ) $array['STATUS'] = "ERROR";
    return  $array;
    }
  public function output($data ) {
    if ( $this->return )return $data;
    else {
      echo drupal_json_encode($data );
      die;
      }
    }
  public function exec($data ) {
    
    $action = $data['action'];  
    if ( defined('EEEEE' ) )
      die($action );
    if ( !method_exists($this , $action ) ) {
       // die(print_r($action, TRUE ) );
      return $this->output($this->error(3 ) );
      }
    else{
      $response = call_user_method($action, $this, $data );
      $this->prepareData($response );
      //die(print_r($response, TRUE ) );
      return $this->output($response );
      }
    }
  public function prepareData(&$data ) {
    //die(print_r($data ) );
    if ( !is_array($data['panel_messages'] ) )
      $data['panel_messages'] = array( );
    if ( !is_array($data['messages'] ) )
      $data['messages'] = array( );
    $failed_count = count($data['failed'] );
    $succes_count = count($data['success'] );
    if ( $failed_count ) {
      $data['panel_messages'][] = $this->formatMessage($failed_count, 'failed', $data['failed'] );
      $data['messages']['failed'] = $data['failed'];
      unset($data['failed'] );
      }
    if ( $succes_count ) {
      $data['panel_messages'][] = $this->formatMessage($succes_count, 'success', $data['success'] );
      $data['messages']['success'] = $data['success'];
      unset($data['success'] );
      }
    $this->prepareDataCallBack($data );
    }
  public function prepareDataCallBack(&$data ) {
    
    }  
  public function formatMessage($count, $type, $data, $simple = FALSE ) {
    $message_plural = $this->plural[$type];
    $action = format_plural($count, $this->plural['action'][0], $this->plural['action'][1] );
    $name = format_plural($count, $this->plural['name'][0], $this->plural['name'][1] );
     // print_r($this->plural, TRUE );die('sss' );
    $return =  array(
      'title' =>   $simple ? $data : $this->plural($type, $count, $action, $name ), 
      'type' =>  ($type == 'failed' ) ? 'error' : 'message', 
      );
    if ( !$simple && property_exists($this, 'theme_panel_message' ) ) {
      //die($this->theme_panel_message );
      $innerHtml = '';
       //  die(print_r($data, TRUE ) );
      foreach ($data as $value ) {
        $innerHtml .= "<li class='EA-message-" . $this->name . "'>" . theme($this->theme_panel_message, array('item' =>   $value ) ) . "</li>";
        }
        
    
      $html = theme('easy_admin_messages', array('ea_class' =>   $this, 'html' =>   $innerHtml ) );
      $return['description'] = $html;
      }
    
    return $return;
    }
  }; 
/*
 * 
 * format_plural($saved_num, 'One menu item saved', '@count menu items saved' ), 
format_plural($saved_num, 'One menu item failed to be saved', '@count menu items saved' ), 
format_plural($message, 'One menu item deleted', '@count menu items deleted' ), 
format_plural($message, 'One menu item failed to be deleted', '@count menu items failed to be deleted' ), 
 */
class EA_ajax_worker_easy_admin_menu extends EA_ajax_worker{
  
  public function __construct($name, $return ) {
    $this->name = $name;
    $this->theme_panel_message = 'easy_admin_message_menu_details';
    $this->return = $return;
    $this->plural['name'] = array(t('menu item' ), t('menu items' ) );
    }  
  public function save($data ) {
    $this->plural['action'] = array(t('saving' ), t('saving' ) );    
    //die(print_r($data, TRUE ). "nnn" );
    if ( isset($data['menu'] ) ) {
      require_once("includes/menu.inc" );
      $menus = $data['menu'];
      $return = array( );
      $allNewMLids = array( ); //storing new items in array for case they have children (plid )
      foreach ($menus as &$menu ) {
      
        $menu = (array ) $menu;
        unset($new_menu );
        //if ( $menu['link_path'] );
        //echo drupal_json_encode($menu );die;
        if ( isset($menu['mlid'] ) ) {
          $old_menu = menu_link_load($menu['mlid'] );
          if ( isset($menu['menu_name'] ) && !($menu['menu_name'] ) ) unset($menu['menu_name'] );
          if ( $old_menu ) {
            $new_menu = array_merge($old_menu, $menu );
            //echo drupal_json_encode($old_menu );die;
            }
          
          }
        $new_menu = isset($new_menu ) ? $new_menu : $menu;
        
        if ( isset($new_menu['link_path'] ) ) {
          $new_menu['link_path'] = $this->check_url($new_menu['link_path'] );
          if ( $new_menu['link_path'] ) {  
            if ( preg_match("/\D/", $new_menu['plid'] ) ) {
              $new_menu['plid'] = isset($allNewMLids["EA-menu-part-" . $new_menu['plid']] ) ? $allNewMLids["EA-menu-part-" . $new_menu['plid']] : 0;
              //echo $allNewMLids[$new_menu['plid']] . ">". $new_menu['link_title'] .  ">"  . $new_menu['plid'];
              }
          //  else echo $new_menu['plid'] . "gggg";
            unset($new_menu['href'] );
            menu_link_save($new_menu );
            //$new_menu['mlid'] =  rand( );
            if ( isset($new_menu['new'] ) )
              $allNewMLids[$new_menu['new']] = $new_menu['mlid'];
            //die(print_r($new_menu ) );
            if ( !$new_menu['justSort'] )
              $result['success'][] = array("item" =>   $new_menu );
            else {
              $result['sorted'] = TRUE;
              unset($new_menu['justSort']);
              }
            //echo render( menu_tree($new_menu['menu_name'] ) );die;///theme('links', array("links" =>   menu_navigation_links( $new_menu['menu_name'] ) ) );die;
            $replace =  easy_admin_get_menu_by_path($new_menu["menu_name"] );
            if ( $replace )
              $result['operations']["replace_all"] = $replace;
            }
          else $result['failed'][] = array("item" =>   $new_menu, "reason" =>   t("Unvalid path" ) );
          }
            
        else $result['failed'][] = array("item" =>   $new_menu, "reason" =>   t("no path" ) );
          
        }
      }
    else
      return $this->output($this->error(2 ) );
    return $result;
    }

  public function delete($data ) {
    $result = array('success' =>   array( ), 'failed' =>   array( ) );
    $this->plural['action'] = array(t('deleting' ), t('deleting' ) );    
    //$result = array( );
    //die(print_r($data ) );
    if ( isset($data['menu'] )&& !empty($data['menu'] ) ) {
      $menus = $data['menu'];
      //die(print_r($menus, TRUE ) );
      foreach ($menus as &$mlid ) {
        $new_menu = (isset($new_menu ) ? $new_menu : menu_link_load($mlid ) );
        menu_link_delete($mlid );
        }
      //print_r($new_menu );
      $query = db_query("SELECT mlid FROM {menu_links} WHERE  mlid IN(:mlids )", array( ':mlids' =>   $menus ) );
    //  $result = $query->execute( );
      foreach ($query as $record ) {
        $result["failed"][] = $record->mlid;
        }
      $result['success'] = array_diff_assoc($menus, $result["failed"] );
      foreach ($result['success']  as $key =>   &$value ) {
        $value= array("item" =>   array("mlid" =>   $value ) );
        }
      
      if ( count($result['success'] ) ) {
        $replace =  easy_admin_get_menu_by_path($new_menu["menu_name"] );
        if ( $replace )
          $result['operations']["replace_all"] = $replace;
        $result['operations']['delete'] = array_diff_assoc($menus, $result["failed"] );
        }
      return $result;
      }
    else return $this->output($this->error(2 ) );
    }

  public function order($data ) {
    $this->plural['action'] = array(t('ordering' ), t('ordering' ) );      
    $result = array( );  
    if ( isset($data['menu'] ) && count($data['menu'] ) ) {
      $menus = $data['menu'];
      foreach ($menus as $mlid =>   $weight )  {
        $new_menu = menu_link_load($mlid );
        $new_menu['weight'] = $weight;
        menu_link_save($new_menu );
        $result['messages']['menus'][] = $new_menu;
        }
      $result['panel_messages'][] =  array(
        'title' =>   t("Order saved" ), 
        "description" =>   '', 
        'type' =>   'message', 
        );
      $result["operations"][] = array("action" =>   "removeClass", "class" =>   "changed" );
      $this->output($result );
      }
    else return $this->output($this->error(2 ) );  
    }

  public static function check_url($url ) {
    global $language;
    $url = urldecode($url );
    $url = preg_replace("%^/%", '', $url );
    
    if ( isset($language->prefix ) ) {
      $url = preg_replace("%^" . $language->prefix . "/?%", '', $url );
      $new_url = drupal_lookup_path('source', $url );
      if ( $new_url )
        return $new_url;
      elseif (drupal_valid_path($url ) ) return $url;  
      }
    //if we are here this is alias, not source, but drupal_lookup_path didn't work.
    // so this is an alias of other language.
    $lans = language_list( );
    unset($lans[$language->language] );//checked already
    foreach ($lans as $lan ) {
      $url = preg_replace("%^" . $lan->prefix . "/?%", '', $url );
      $new_url = drupal_lookup_path('source', $url, $lan->language );
      if ( $new_url )return $new_url;
      }
    //nothing worked...
    return FALSE;
    }
  public function prepareDataCallBack(&$data ) {
    
    if ( $data['sorted'] )
      $data['panel_messages'][]  = $this->formatMessage(0, 'menu', t('New order saved' ), TRUE );
    //die(print_r($data ) );
    }  
  }

class EA_ajax_worker_easy_admin_node extends EA_ajax_worker{
  public function __construct($name, $return = TRUE ) {
    $this->name = $name;
    $this->theme_panel_message = 'easy_admin_message_node_details';
    $this->return = $return;
    $this->plural['name'] = array(t('node item' ), t('node items' ) );
    }  
      
  public function save($data ) {
    $this->plural['action'] = array(t('saving' ), t('saving' ) );
    $result = array('success' =>   array( ), 'failed' =>   array( ) );
    if ( !isset($data['node'] ) || empty($data['node'] ) ) {
    //print_r($data );
      return easy_admin_error(3 );
      }
    else{
      $nodes = $data['node'];
      foreach ($nodes as $node ) {
        //print_r($node );
        if ( isset($node['nid'] ) && $node['nid'] ) {
          $oldNode = node_load($node['nid'] );
          //print_r("fff" .$node['nid'].$oldNode );
          }
        elseif ( !isset($node['type'] ) ) {
          $result['failed'][] = array("reason" =>   t("no type declared" ), "data" =>   $oldNode );
          }
        else{
          $oldNode = new stdClass( );
          $oldNode->type = $node['type'];
          node_object_prepare($oldNode );
          }
        foreach (array("title", "promote", "published", "sticky" ) as $value ) {
          if ( isset($node[$value] ) )$oldNode->{$value} = $node[$value];
          }
        
        //print_r($oldNode );
        if ( is_object($oldNode ) ) {
          node_save($oldNode );
          $result['success'][] = $oldNode;
          }
        }
      return $result;
      }
    }
  public function delete($data ) {
    $result = array("success" =>   array( ), "failed" =>   array( ), "operations" =>   array("delete" =>   array( ) ), "type" =>   "node" );
    $this->plural['action'] = array(t('deleting' ), t('deleting' ) );
    if ( !isset($data['node'] ) || empty($data['node'] ) ) {
      //print_r($data );
      return $this->error(2 );
      }
    $nids = $data['node'];
    //print_r($nids );die;
    foreach ($nids as $nid ) {
      node_delete($nid );
      $result['success'][] =  array("nid" =>   $nid );
      $result['operations']['delete'][] =  array("nid" =>   $nid );

      }
    return $result;
    }
  }

class EA_ajax_worker_easy_admin_view extends EA_ajax_worker{
  public function __construct($name, $return = TRUE ) {
    $this->name = $name;
    $this->theme_panel_message = 'easy_admin_message_view_details';
    $this->return = $return;
    $this->plural['name'] = array(t('view page' ), t('view pages' ) );
    }
  public function save($data ) {
    $this->plural['action'] = array(t('saving' ), t('saving' ) );
    $response = array(
      'panel_messages' =>   array( ), 
      'success' =>   array( ), 
      'failed' =>   array( ), 
      );
    if ( !isset($data['view'] ) || empty($data['view'] ) ) {
      //print_r($data );
      return $this->error(2 );
      }
    foreach ($data['view'] as $value ) {
      $view = views_get_view($value['view'], $value['display'] );
      if ( !isset($view->display[$value['display']]->display_options ) )
        $view->display[$value['display']]->display_options = array('defaults' =>   array('title' =>   FALSE ) );
      elseif ( !isset($view->display[$value['display']]->display_options['defaults'] ) )
        $view->display[$value['display']]->display_options['defaults'] = array('title' =>   FALSE );
      else $view->display[$value['display']]->display_options['defaults']['title'] = FALSE;
      $view->display[$value['display']]->display_options['title'] = $value['title'];
      $is_saved = views_save_view($view );
      //if ( $is_saved )
      $response['success'][] =$value;
      //else $result['messages']['failed'][] =$value;
      }
    return $response;
    }
  }

class EA_ajax_worker_easy_admin_comment extends EA_ajax_worker{
  public function __construct($name, $return = TRUE ) {
    $this->name = $name;
    $this->theme_panel_message = 'easy_admin_message_comment_details';
    $this->return = $return;
    $this->plural['name'] = array(t('comment' ), t('comments' ) );
    }
  public function save($data ) {
    $this->plural['action'] = array(t('saving' ), t('saving' ) );
    $result = array("messages" =>   array("success" =>   array( ), "failed" =>   array( ) ), "type" =>   "comment" );
    if ( !isset($data['comment'] ) || empty($data['comment'] ) ) {
      return $this->error(2 );
      }
    else{
      $comments = $data['comment'];
      foreach ($comments as $comment ) {
        
        if ( isset($comment['cid'] ) && $comment['cid'] ) {
          $old_comment = comment_load($comment['cid'], TRUE );
          //print_r("fff" .$node['nid'].$oldNode );
          }
        //print_r($old_comment );die;
        foreach (array("status" ) as $value ) {
          if ( isset($old_comment->{$value} ) )$old_comment->{$value} = $comment[$value];
          }
        
        //print_r($oldNode );
        if ( is_object($old_comment ) ) {
          comment_save($old_comment );
          $result['success'][] = $old_comment;
          }
        }
      return $result;
      }
    }
    public function delete($data ) {
      $this->plural['action'] = array(t('deleting' ), t('deleting' ) );
      $result = array("messages" =>   array("success" =>   array( ), "failed" =>   array( ) ), "type" =>   "Comment" );
      if ( !isset($data['comment'] ) || empty($data['comment'] ) ) {
        return $this->error(2 );
        }
      else{
        $comments = $data['comment'];
        //print_r($comments );die;
        $check_cids = array( );
        foreach ($comments as $comment ) {
          comment_delete($comment );  
          $check_cids[$comment] = $comment;
          }
        
        $check = db_query("SELECT cid FROM {comment} WHERE cid IN (:cids )", array(':cids' =>   $check_cids ) );
        $failed = array( );
        while ($check_cid = $check->fetchColumn( ) ) {
          //print_r($check_cid );
          $result['failed'][] = array('cid' =>   $check_cid );
          //unset($check_cids[$check_cid['cid']] );
          }
        //print_r($check_cids );die;
        
        foreach ($check_cids as $cid ) {
          $result['success'][] = array('cid' =>   $cid );
          }
        return $result;
        }
      }
    }  

class EA_ajax_worker_easy_admin_term extends EA_ajax_worker{
  public function __construct($name, $return = TRUE ) {
    $this->name = $name;
    $this->theme_panel_message = 'easy_admin_message_term_details';
    $this->return = $return;
    $this->plural['name'] = array(t('term' ), t('terms' ) );
    }
  public function save($data ) {
    $this->plural['action'] = array(t('saving' ), t('saving' ) );
    $result = array("messages" =>   array("success" =>   array( ), "failed" =>   array( ) ), "type" =>   "term" );
    if ( !isset($data['term'] ) || empty($data['term'] ) ) {
      //print_r($data );
      return $this->error(2 );
      }
    else{
      $terms = $data['term'];
      foreach ($terms as $term ) {
        
        if ( isset($term['tid'] ) && $term['tid'] ) {
          $old_term = taxonomy_term_load($term['tid'], TRUE );
          }
        foreach (array("name" ) as $value ) {
          if ( isset($old_term->{$value} ) )$old_term->{$value} = $term['title'];
          }
        if ( is_object($old_term ) ) {
          taxonomy_term_save($old_term );
          $result['success'][$old_term->tid] = $old_term;
          }
        }
      return $result;
      }
    }
  public function delete($data ) {
    $this->plural['action'] = array(t('deleting' ), t('deleting' ) );
    $result = array("messages" =>   array("success" =>   array( ), "failed" =>   array( ) ), "type" =>   "term" );
    if ( !isset($data['term'] ) || empty($data['term'] ) ) {  
      return $this->error(2 );
      }
    else{
      $terms = $data['term'];
      //print_r($terms );die;
      $check_tids = array( );
      foreach ($terms as $term ) {
        taxonomy_term_delete($term );  
        $check_tids[$term] = $term;
        //$result['success'][$term] = $term;
        }
      
      $check = db_query("SELECT tid FROM {taxonomy_term_data} WHERE tid IN (:tids )", array(':tids' =>   $check_tids ) );
      $failed = array( );
      while ($check_tid = $check->fetchColumn( ) ) {  
        $result['failed'][$check_tid['tid']] = array('tid' =>   $check_tid['tid'] );
        unset($check_tids[$check_tid['tid']] );
        }
    
      foreach ($check_tids as $tid ) {
        $result['success'][] = array('tid' =>   $tid );
        //unset($result['success'][$tid ] );
        }
    //    print_r($result['success'] );  die;
      return $result;
      }
    }
  }

class EA_ajax_worker_easy_admin_panel extends EA_ajax_worker{
  public function all($data ) {//collect all panel calls
    $allData = $data['alldata'];
    $allResponse = array( ); 
    foreach ($allData as $module =>   $moduleOp ) {
    if ( !isset($allResponse[$module] ) )$allResponse[$module] = array( );
      foreach ($moduleOp as $action =>   $objType ) {
        if ( !isset($allResponse[$module][$action] ) )$allResponse[$module][$action] = array( );
        foreach ($objType as $typeName =>   $data ) {
          $dataToSend = array(
            'action' =>   $action, 
            'objType' =>   $typeName, 
            'module' =>   $module, 
            //'panelData' =>   $data, 
            );
          //die(print_r($dataToSend, TRUE ) );
          $dataToSend[$typeName] = $data;
          
          $allResponse[$module][$action][$typeName ] = easy_admin_all($dataToSend, TRUE );
          }
        }
      }
//    die('bb' );
    return $this->output($allResponse );
    }
  
  }  

  
/// messages theme functions


function theme_easy_admin_messages($vars ) {
  $obj = $vars['ea_class'];
  $html .= "<ol class='EA-panel-message-{$obj->name}'>";
  $html .= $vars['html'];
  $html .= "</ol>";
  //print_r($vars );
  return $html;
  }
function theme_easy_admin_message_menu_details($vars ) {
  $item = (array ) $vars['item'];
  if ( isset($item['item'] ) )$item = $item['item'];
 // print_r($item );
  $text = t('Menu item number @mlid. Menu title: @title, Menu href: @link', array(
    '@mlid' =>   $item['mlid'], 
    '@title' =>   $item['title'], 
    '@link' =>   isset($item['href']) && $item['href'] ? $item['href'] : $item['link_path'], 
    ) );
  return $text;
  }

function theme_easy_admin_message_node_details($vars ) {
  $node = $vars['item'];
  $text = t('Node item number @nid. Title: @title', array(
    '@nid' =>   $node->nid, 
    '@title' =>   $node->title, 
    )
  );
  return $text;
  }

function theme_easy_admin_message_comment_details($comment ) {
  
  $comment = (array ) $comment['item'];  
  $text = t('Comment item number @cid.', array('@cid' =>   $comment['cid'] ) );
  if ( isset($comment['subject'] ) )
    $text .= t('Title: @title', array('@title' =>   $comment['subject'], ) );
  return $text;
  }

function theme_easy_admin_message_view_details($item ) {
  $text = t("View name @view. display: @display title changed to '@title'", array(
    '@view' =>   $item['view'], 
    '@display' =>   $item['display'], 
    '@title' =>   $item['title'], 
    ) );
  return $text;
  }

function theme_easy_admin_message_term_details($item ) {
  //die(print_r($item, TRUE ) );
  $term = (array ) $item['item'];  
  $text = t('Term item number @tid.', array('@tid' =>   $term['tid'] ) );
  if ( isset($term['name'] ) )
    $text .= t('name: @name', array('@name' =>   $term['name'], ) );
  return $text;
  }

  
function easy_admin_builder($module, $type, $return ) {
  $class_name = 'EA_ajax_worker_' . $module . "_" . $type;
  
  if ( class_exists($class_name ) ) {
    return new $class_name($type, $return );
    }
  //else return EA_ajax_worker::output(EA_ajax_worker::error(array("CODE" =>   3, "TEXT" =>   t("Action for type not found" ) ) ) );
  else return FALSE;
  }

function easy_admin_error($data ) {
  $error = new EA_ajax_worker_easy_admin_node('node' ); 
  return $error->output($error->error($data ) );
  }  
