<?php

function theme_easy_admin_get_views_pages($links ) {
  $html = "<ul>";  
  foreach ($links as $value ) {
    $html .= "<li>";  
    $html .= "<div class='EA-views-pages-link'>" . $value['link']  . "</div>";
    $html .= "<div class='EA-views-pages-desc'>" . $value['description'] . "</div>";
    $html .= "</li>";  
    }
  $html .= "</ul>";
  return $html;  
  }

function theme_easy_admin_message_screen( ) {
  return "<div id='EA-panel'>
      <div id='EA-panel-inner'>
        <div class='EA-screen'>
          <div class='EA-screen-text'></div>
        </div>
        <div class='EA-buttons-area'></div>
        <div class='EA-panel-arrow open'><div class='EA-panel-arrow-out'></div><div class='EA-panel-arrow-in'></div></div>
      </div>
    </div>";
  }

function theme_easy_admin_edit_box($form ) {
    $edit_box = "<div id='EA-edit-box'>";
    $edit_box .=     "<h3>" . t("Edit Link" ) . "</h3>";
    $edit_box .=     "<div class='EA-edit-box'>";
    $edit_box .=     render($form );
    $edit_box .=     "</div>";
    $edit_box .= "</div>";
    return $edit_box;
    }

function theme_easy_admin_menu_all($vars ) {
    //dpm($vars );
    $attr = array("class" =>   array("" . EA_PREFIX . "menu-edit" ), "id" =>   array("" . EA_PREFIX . "menu-edit-" . $vars['name'] ) );
    $html = "<div " . drupal_attributes($attr ) . ">";
    $html .= "<ul class='" . EA_PREFIX . "menus-parts " . EA_PREFIX . "menus-parts-all clearfix' >";
    $html .= theme('easy_admin_menu_parts', array('menu_part' =>   $vars['menu_tree'] ) );
    $html .= "</ul>";
    $html .= "</div>";
    return $html;
    }

function theme_easy_admin_menu_all_wrp($vars ) {
  return '<div id=' . EA_PREFIX . 'menu-edit-all-select" >' . $vars['select']. '</div><div id="' . EA_PREFIX . 'menu-edit-all" >' . $vars['html'] ."</div>";  
  }        
  
function theme_easy_admin_menu_parts($vars ) {
    $html = "";
    
    foreach ($vars['menu_part'] as $menu_part ) {
      $link = $menu_part['link'];
      
      $attr = array(
        "class" =>   array("" . EA_PREFIX . "menu-part" , 
           EA_PREFIX . "menu-part-" . $link['mlid'], 
          // EA_PREFIX . 'data-wrp', 
          'clearfix', 
          )
        , "id" =>   array("" . EA_PREFIX . "menu-part-" . $link['mlid'] ) );
      if ( $link['external'] && $link['link_path'] != '<front>' )  $attr['class'][] = ("" . EA_PREFIX . "menu-part-external" );
      if ( !$link['expanded'] )  $attr['class'][] = ("" . EA_PREFIX . "menu-part-not-expanded" );
      if ( $link['hidden'] )  $attr['class'][] = ("" . EA_PREFIX . "menu-part-hidden" );
      if ( isset($link['extra'] ) && isset($menu_part['extra']['type'] ) )
        $attr['class'][] = ("" . EA_PREFIX . "menu-part-" . $menu_part['extra'] );
      $html .= "<li " . drupal_attributes($attr )  . ">";
      $html .= "<div class='" . EA_PREFIX . "no-ul-wrp'><div class='" . EA_PREFIX . "handles-wrp clearfix'><span class='" . EA_PREFIX . "handle icon icon-move'></span><span class='" . EA_PREFIX . "handle " . EA_PREFIX . "plus icon icon-minus-sign-alt showing" . (empty($menu_part['below'] ) ? " " . EA_PREFIX . "hidden" : "" ) . "' ></span></div>";
      $html .= "<div class='" . EA_PREFIX . "menu-part-data-wrp clearfix'>";
      $html .= "<div class='" . EA_PREFIX . "data-wrp " . EA_PREFIX . "menu-part-menu-data-wrp clearfix'>";
      $html .= "<div class='" . EA_PREFIX . "action-bar'>
            <span class='icon-plus " . EA_PREFIX . "icon' title='" . t('add new link' ) . "'></span>
            <span class='icon-trash " . EA_PREFIX . "icon'  title='" . t('delete link' ) . "'></span>
            <span class='icon-undo " . EA_PREFIX . "icon'  title='" . t('revert changes' ) . "'></span>
          </div>";
      $html .= "<h3 ><div class='span-input-wrp'><span class='" . EA_PREFIX ."title span-input " . EA_PREFIX ."dataClass-data-link_title'>{$link['link_title']}</span></div></h3>";
      //$html .= "<div class='" . EA_PREFIX . "menu-part-link-title'></div>";
      $html .= "<div class='" . EA_PREFIX . "menu-part-link-path '><div class='span-input-wrp'><span class='" . EA_PREFIX ."title span-input " . EA_PREFIX ."dataClass-data-link_path'>" . ($link['link_path'] == "<front>" ? htmlentities($link['link_path'] )  : $link['link_path'] ) . "</span></div></div>";
      $html .= "<div class='" . EA_PREFIX . "link " . EA_PREFIX . "link-statuses'>";
      foreach (array("hidden", "expanded" ) as $value ) {
        if ( $value == 'hidden' ) {
          $iconClass =  'icon-check';
          $toggle = array('published', 'hidden' );
          }
        else{
          $iconClass ="icon-sitemap";
          $toggle = array('not expanded', 'expanded' );
          }
        //$iconClass = ($value == 'hidden' ? 'af-circle' :"icon-sitemap" );
        $html .= "<div class='" . EA_PREFIX . "link " . EA_PREFIX . "link-toggle " . EA_PREFIX . "link-statuses-part " . EA_PREFIX . "dataClass-data-$value " . EA_PREFIX . ($link[$value] ? 'on' : "off" ) ."'>
          <i class='icon $iconClass " . EA_PREFIX . "off-text' title='{$toggle[0]}'></i> 
          <i class='icon $iconClass " . EA_PREFIX . "on-text' title='{$toggle[1]}'></i> 
        </div>";
        }
      
      /*foreach (array("hidden", "expanded" ) as $value ) {
        $html .= "<span class='" . EA_PREFIX . "link " . EA_PREFIX . "link-toggle " . EA_PREFIX . "link-statuses-part " . EA_PREFIX . "dataClass-data-$value " . EA_PREFIX .  ($link[$value] ? 'on' : "off" ) . " " . ($value == 'hidden' ? 'icon-circle' :"" ) . "'></span>";
        }*/
      //die(print_r($link ) );
      $html .=  easy_admin_data_container($link, 'menu', 'mlid', 'easy_admin', array("mlid", "external", "link_title", "expanded", "hidden", "link_path" ) );
      $html .= "</div>";
      $html .= "</div>";
      if ( isset($link['extra'] ) )
        $html .=  theme('easy_admin_menu_extra', array('extra' =>   $link['extra'] ) ) ;
      $html .= "</div></div>";
      if ( !empty($menu_part['below'] ) )
        $html .=  "<ul class='" . EA_PREFIX . "menus-parts'>" . theme('easy_admin_menu_parts', array('menu_part' =>   $menu_part['below'] ) ). "</ul>";
      $html .= "</li>";
      }
    return $html;
    }

function theme_easy_admin_menu_extra($vars ) {
  //dpm($vars );
  $extra = $vars['extra'];
  if ( !isset($extra['type'] ) )return "";
  $title  = $user= "";
  $wrap_class = array(
    EA_PREFIX . "menu-part-extra", 
    EA_PREFIX . "menu-part-" . $extra['type'], 
    EA_PREFIX. "data-wrp", 
    "clearfix", 
    );
  switch ( $extra['type']) {
    case 'view':
      $title = $extra['title'];
      $wrap_class[] = EA_PREFIX . "menu-part-view-" . $extra['args']['view'];
      $wrap_class[] = EA_PREFIX . "menu-part-display-" . $extra['args']['display'];
      break;
    case 'contact':
      $title = t('contact' );
      break;
    case 'term':
     $title = $extra['name'];
     $wrap_class[] =  EA_PREFIX . "tid-" . $extra['tid'];
      break;
    case 'node':
      $title = $extra['node_title'];
      $user = l($extra['users_node_name'], "user/" . $extra['users_node_uid'] );
      $wrap_class[] = EA_PREFIX . "menu-part-node-" . $extra['node_type'];
      $wrap_class[] =  EA_PREFIX . "menu-part-node-" . $extra['nid'];
      break;
      }
  
  $html = "<div class='" . EA_PREFIX . "menu-part-separator" . ($vars['separator'] ? "'><span class='EA-link-text'>" . $vars['separator']  . "</span>":  " icon-link'>" ) . "</div>";
  $html .= "<div " . drupal_attributes(array("class" =>   $wrap_class ) ) . ">
      <div class='EA-action-bar'>
        <span class='icon-trash EA-icon' title='" . t('delete node' ) .  "'></span>
        <span class='icon-undo EA-icon' title='" . t('revert changes' ) .  "'></span>
      </div>";
  $html .= "<h3 >". ($title ? "<div class='span-input-wrp'><span class='" . EA_PREFIX ."dataClass-data-title span-input'>$title</span></div>" : "" ) . ($user ?  "<span class='" . EA_PREFIX . "node " . EA_PREFIX . "node-author'>$user</span>" : "" ) . "</h3>";
  switch($extra['type'] ) {
    case 'external':
      $html .= '<span class="' . EA_PREFIX . 'break-word">' . l($extra['url'],$extra['url']) . '</span>';
      break;
    case 'view':
    case 'contact':
    case 'term':
      $html .= l(t('Edit' ), $extra['edit_link'] );
      break;
    case "node":
      $replace = $extra['html'];
      $replace_text = "" ;
      
      if ( isset($extra['real_count'] ) || isset($extra['node_comment_statistics_comment_count'] ) ) {
        $real_count = isset($extra['real_count'] ) ? $extra['real_count'] : 0;
        $app_count = isset($extra['node_comment_statistics_comment_count'] ) ? $extra['node_comment_statistics_comment_count'] : 0;
        $sum = $real_count - $app_count;
        //dpm($extra );
        //dpm(array($app_count, $real_count, $sum ) );
        if ( $real_count ) {
          $replace_text .= "<div class='" . EA_PREFIX . "node icon-comments'><a href='#' >";
          if ( $app_count )
            $replace_text .="<span class='approved'>$app_count</span>";
          if ( $app_count && $sum )
            $replace_text .= "/";
          if ( $sum )
            $replace_text .= "<span class='unapproved'>$sum</span>";
          $replace_text .= "</a></div>";
          }
        }
      $html .= preg_replace("/<!--comment_link_count-->/", $replace_text, $replace );
      break;
    }
  if ( $extra['type'] == 'view' ) {
    $data = $extra['args'] + array('title' =>   $title );
    $html .= easy_admin_data_container($data, 'view', 'view' );
    }
  elseif ( $extra['type'] == 'term' ) {
    $data = $extra['args'] + array('title' =>   $title, 'tid' =>   $extra['tid'] );
    $html .= easy_admin_data_container($data, 'term', 'tid' );
    }
  $html .="</div>";
  return $html;
  }

function theme_easy_admin_panel_bottom_buttons( ) {
  $html = '<div class="' . EA_PREFIX . 'panel-bottom-buttons">
        <a class="' . EA_PREFIX . 'save-all ' . EA_PREFIX . 'panel-bottom-button" href="#">'. t('save' ) . '<i class="icon-save ' . EA_PREFIX . 'icon"></i></a>
        <a class="' . EA_PREFIX . 'add-new ' . EA_PREFIX . 'panel-bottom-button" href="#">'. t('Add' ) . '<i class="icon-plus ' . EA_PREFIX . 'icon"></i></a>
        <a class="' . EA_PREFIX . 'copy ' . EA_PREFIX . 'panel-bottom-button" href="#">'. t('copy' ) . '<i class="icon-copy ' . EA_PREFIX . 'icon"></i></a>
      </div>';
  return $html; 
  }
