<?php

//print_r($fields );
$html ="";
//foreach ($fields as $key =>   $value ) {
//$html .=$key .": ". $value->content . "<br/>";  
//}

//$html .= "<div class='" . EA_PREFIX . "node " . EA_PREFIX . "node-title'>{$fields['title']->raw}</div>";
//$html .= "<div class='" . EA_PREFIX . "node " . EA_PREFIX . "node-author'>". l($fields['name']->raw, "user/" . $fields['uid']->raw ) . "</div>";
$html .= "<div class='" . EA_PREFIX . "node " . EA_PREFIX . "node-edit'>". l("<span >" . t('Edit' ) . "</span>", "node/" . $fields['nid']->raw ."/edit", array("html" =>   TRUE, 'attributes' =>   array('class' =>   array(EA_PREFIX . "link-edit-node" ) ) ) ) . "</div>";
$html .= "<div class='" . EA_PREFIX . "node " . EA_PREFIX . "node-statuses'>";
foreach (array("status", "sticky", 'promote' ) as $value ) {
    if ( !isset($fields[$value]->content ) )continue;
    
    if ( $value == 'status' ) {
      $iconClass =  'icon-eye-open';
      $toggle = array( 'unpublished', 'published' );
      }
    else{
      $iconClass = $value == 'sticky' ?  "icon-star" : 'icon-home';
      $toggle = array( 'not ' . $value, $value, );
      }
    //$iconClass = ($value == 'hidden' ? 'af-circle' :"icon-sitemap" );
    $html .= "<div class='" . EA_PREFIX . "link " . EA_PREFIX . "link-toggle " . EA_PREFIX . "link-statuses-part " . EA_PREFIX . "dataClass-data-$value " . EA_PREFIX . ($fields[$value]->raw ? 'on' : "off" ) ."'>
      <i class='icon $iconClass " . EA_PREFIX . "off-text' title='{$toggle[0]}'></i> 
      <i class='icon $iconClass " . EA_PREFIX . "on-text' title='{$toggle[1]}'></i> 
    </div>";
    }

$html .= "</div>";
//if ( isset($fields['comment_count']->raw ) && $fields['comment_count']->raw )
$html .= "<!--comment_link_count-->";
echo $html;

$data_array = array( );

foreach ($fields as $key =>   $value ) {
  $data_array[$key] = $value->raw;
  }
echo easy_admin_data_container($data_array, 'node', "nid", 'easy_admin', FALSE );
  


