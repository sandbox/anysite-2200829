<?php

//print_r($fields );
$html ="<div class='" . EA_PREFIX . "data-wrp " . EA_PREFIX . "comment " . EA_PREFIX . "cid-" . $fields['cid']->raw . " " .  ($fields['status']->raw ? "published" : "unpublished" ) . "'>";
$html .= "<div class='" . EA_PREFIX . "action-bar'>
      <span class='icon-trash " . EA_PREFIX . "icon'  title='" . t('delete comment' ) . "'></span>
      <span class='icon-undo " . EA_PREFIX . "icon'  title='" . t('revert changes' ) . "'></span>
      </div>";
$html .= "<div class='" . EA_PREFIX . "comment " . EA_PREFIX . "comment-title'>" . $fields['subject']->content . " </div>";
$html .= "<div class='" . EA_PREFIX . "comment " . EA_PREFIX . "comment-name'>" . $fields['name']->content . " </div>";
$html .= "<div class='" . EA_PREFIX . "comment " . EA_PREFIX . "comment-body'>" . $fields['comment_body']->content . " </div>";

$html .= "<div class='" . EA_PREFIX . "comment " . EA_PREFIX . "comment-statuses'>";
$iconClass =  'icon-eye-open';
$toggle =  array( 'unpublished', 'published' );
$html .= "<div class='" . EA_PREFIX . "comment " . EA_PREFIX . "comment-edit'>". l("<span >" . t('Edit' ) . "</span>", "comment/" . $fields['cid']->raw ."/edit", array("html" =>   TRUE, 'attributes' =>   array('class' =>   array(EA_PREFIX . "link-edit-comment" ) ) ) ) . "</div>";
$html .= "<div class='" . EA_PREFIX . "link " . EA_PREFIX . "link-toggle " . EA_PREFIX . "link-toggle-comment " . EA_PREFIX . "link-statuses-part " . EA_PREFIX . "dataClass-data-status " . EA_PREFIX . ($fields['status']->raw ? 'on' : "off" ) ."'>
  <i class='icon $iconClass " . EA_PREFIX . "off-text' title='{$toggle[0]}'></i> 
  <i class='icon $iconClass " . EA_PREFIX . "on-text' title='{$toggle[1]}'></i> 
</div>";

$html .= "</div>";




$data_array = array( );

foreach ($fields as $key =>   $value ) {
  $data_array[$key] = $value->raw;
  }
//dpm($data_array );
$html .=  easy_admin_data_container($data_array, 'comment', "cid", 'easy_admin', FALSE, array('comment_body' ) );
$html .= "</div>";  
echo $html;

